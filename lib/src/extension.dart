import 'dart:math';
import 'dart:ui' show lerpDouble;

import 'package:flutter/painting.dart' show Color;
import 'package:okshop_common/okshop_common.dart';

import 'constants.dart';
import 'models/addition_product.dart';
import 'models/coupon.dart';
import 'models/invoice.dart';
import 'models/item.dart';
import 'models/member.dart';
import 'models/order_selector_filter.dart';
import 'models/order_summary.dart';
import 'models/order_total.dart';
import 'models/pagination.dart';
import 'models/product_info.dart';
import 'models/receipt.dart';
import 'models/revenue.dart';
import 'models/setting_label.dart';
import 'models/table.dart';
import 'models/table_req.dart';
import 'utils.dart';

extension ExtensionAdditionProduct on AdditionProduct {
  // 組成方便顯示用的按鈕字串
  String orderSelectionButtonText() {
    price ??= 0;
    if (price != 0) {
      return '$name(${price.decimalStyle})';
    }
    return name;
  }
}

extension ExtensionInvoice on Invoice {
  // 取得一般商品 (不含額外費用、服務費)
  Iterable<Item> get normalItems {
    items ??= <Item>[];
    return items.where((element) => element.itemType.isNormal ?? false);
  }

  // 一般商品數量總和
  num get normalItemsCount {
    return normalItems.fold<num>(0, (previousValue, element) {
      return previousValue + element.nnQuantity;
    });
  }

  // 計算稅別
  void refresh() {
    // 混合稅率條件: 應稅銷售額及免稅銷售額必須同時有值
    if (itemsTXSalesAmount > 0 && itemsFreeSalesAmount > 0) {
      taxType = BpscmTaxType.Mix.value;
    } else {
      // 不是應稅就是免稅
      if (itemsFreeSalesAmount > 0) {
        taxType = BpscmTaxType.Free.value;
      } else {
        // 應稅
        taxType = BpscmTaxType.TX.value;
      }
    }
    items.forEach((element) => element.taxRate = taxRate);
  }

  bool get isTaxTypeMix => taxType.bpscmTaxType.isMix;

  // 顯示稅額的條件: 有購買人或混合稅率
  bool get showTax {
    // HACK:
    // return true;
    return hasBuyer || isTaxTypeMix;
  }

  bool get hasBuyer {
    // HACK:
    // return true;
    return (buyer != null && buyer.isNotEmpty);
  }

  bool get isPrinted {
    // HACK:
    // return true;
    printMark ??= 0;
    return printMark != 0;
  }

  set isPrinted(bool value) => printMark = value ? 1 : 0;

  // 已經列印過紙本，需註記補印
  String get displayPrintMark {
    final mark = isPrinted ? '補印' : '';
    return '電子發票證明聯$mark';
  }

  // 加上補印的字體較小
  // SunmiSize get printMarkSize => isPrinted ? SunmiSize.xl : SunmiSize.xxl;

  num get _twYear => dateTime == null ? 0 : dateTime.year - 1911;

  String get displayTwDateTime =>
      dateTime != null ? dateTime.displayTwDateTime : '';

  String get displayInvoiceNumber {
    this.invoiceNumber ??= '';
    // AB
    final regexpA = RegExp(r'^[A-Z]{2}');
    final matchA = regexpA.firstMatch(invoiceNumber);
    final newTextA = matchA?.group(0) ?? '';
    // 12345678
    final regexp0 = RegExp(r'[0-9]{1,8}');
    final match0 = regexp0.firstMatch(invoiceNumber);
    final newText0 = match0?.group(0) ?? '';
    //
    final newText = '$newTextA-$newText0';
    return newText;
  }

  String get displayDate => dateTime?.y_MM_dd ?? '';

  String get displayDateTime => dateTime?.y_MM_ddHHmmss ?? '';

  // 有買方須顯示 '格式25'
  String get displayTaxType => true == hasBuyer ? '格式25' : '';

  String get displayRandomNumber {
    randomNumber ??= '';
    return '隨機碼$randomNumber';
  }

  String get displayTotalAmount => '總計${itemsTotalAmount.decimalStyle}';

  String get displayBuyer => hasBuyer ? '買方$buyer' : '';

  String get displaySeller => '賣方$seller';

  String get barcode {
    if (dateTime == null) {
      return '';
    }
    final mm = dateTime.month;
    final even = mm % 2 == 0 ? mm : mm + 1;
    final evenMonth = '$even'.padLeft(2, '0');
    return '$_twYear$evenMonth$invoiceNumber$randomNumber';
  }

  String get fixedString {
    final value1 = invoiceNumber; // 發票字軌 ZJ71292204
    final value2 = _twYYYMMdd; // 發票開立日期 1090312
    final value3 = randomNumber; // 隨機碼 3612
    final value4 = _totalWithoutTaxHex; // 銷售額 00000055 (85)
    final value5 = _totalWithTaxHex; // 總計額 00000059 (89)
    final value6 = _buyer; // 買方統一編號 00000000
    final value7 = seller; // 賣方統一編號 42308086
    final value8 = _getEncryptedString(kAESKey); // 加密驗證資訊
    return '$value1$value2$value3$value4$value5$value6$value7$value8';
  }

  String get leftString {
    // final value1 = this.invoiceNumber; // 發票字軌 ZJ71292204
    // final value2 = this._twYYYMMdd; // 發票開立日期 1090312
    // final value3 = this.randomNumber; // 隨機碼 3612
    // final value4 = this._totalHexWithoutTax; // 銷售額 00000055 (85)
    // final value5 = this._totalHexWithTax; // 總計額 00000059 (89)
    // final value6 = this._buyer; // 買方統一編號 00000000
    // final value7 = this.seller; // 賣方統一編號 42308086
    // final value8 = this._getEncryptedString(kAESKey); // 加密驗證資訊
    final value9 = _customField; // 營業人自行使用區 **********
    final leftCount = getMaxItemsCount(kRemainLength);
    final rightCount = getMaxItemsCount(kQrCodeLength - 2, leftCount);
    final value10 = leftCount + rightCount; // 二維條碼記載完整品 3
    final value11 = _itemCount; // 該張發票交易品目總筆數 3
    final value12 = _stringEncode; // 中文編碼參數 2 (base64)
    final value13 = getItemBase64String(0, leftCount);
    //
    final sector1 = fixedString;
    final sector2 = ':$value9:$value10:$value11:$value12:$value13';
    return '$sector1$sector2';
  }

  String get leftQrString => leftString.padRight(128, ' ');

  num getMaxItemsCount(num length, [int start = 0]) {
    num count = 0;
    String value1 = '';
    items ??= <Item>[];
    for (var i = start; i < items.length; i++) {
      final item = items.elementAt(i);
      final value2 = '$value1:${item.invoiceString}';
      final value3 = Utils.getBase64String(value2);
      if (value3.length > length) {
        break;
      }
      value1 = value2;
      count++;
    }
    return count;
  }

  // String get _itemString {
  //   final count = this.getMaxItemsCount(kRemainLength);
  //   final value1 = this.items.take(count).fold('',
  //       (previousValue, element) => '$previousValue:${element.invoiceString}');
  //   return SunmiUtil.getBase64String(value1);
  // }

  String getItemBase64String(num start, num end) {
    final value1 = items.getRange(start, end).fold<String>(
      '',
      (previousValue, element) {
        if (previousValue.isEmpty) {
          return element.invoiceString;
        }
        return '$previousValue:${element.invoiceString}';
      },
    );
    return Utils.getBase64String(value1);
  }

  num get _itemCount {
    items ??= <Item>[];
    return items.length;
  }

  String get rightString {
    final value1 = '**';
    final leftCount = getMaxItemsCount(kRemainLength);
    final rightCount = getMaxItemsCount(kQrCodeLength - 2, leftCount);
    final value2 = rightCount > 0
        ? getItemBase64String(leftCount, leftCount + rightCount)
        : '';
    return '$value1$value2';
  }

  String get rightQrString => rightString.padRight(128, ' ');

  String get _twYYYMMdd {
    final y = _twYear;
    final m = '${dateTime.month}'.padLeft(2, '0');
    final d = '${dateTime.day}'.padLeft(2, '0');
    return '$y$m$d';
  }

  // 補零後的 buyer
  String get _buyer => (this.buyer ?? '').padLeft(8, '0');

  /// 中文編碼參數 (1 碼)：定義後續資訊的編碼規格
  /// 0: big5
  /// 1: uft8
  /// 2: base64
  String get _stringEncode => '2';

  ///
  /// 營業人自行使用區 (10 碼)：提供營業人自行放置所需資訊，若不使用則以 10 個“*”符號呈現。
  ///
  String get _customField => ''.padLeft(10, '*');

  String _getEncryptedString(String aesKey) {
    return Utils.getEncryptedString(
      aesKey: aesKey,
      invoiceNumber: invoiceNumber,
      randomNumber: randomNumber,
    );
  }

  // 總額16進位
  String get _totalWithTaxHex {
    final itemsTotalAmountRound = itemsTotalAmount.round();
    final t = itemsTotalAmountRound.toRadixString(16);
    final up = t.toUpperCase();
    return up.padLeft(8, '0');
  }

  // 銷售額16進位
  String get _totalWithoutTaxHex {
    final itemsSalesRound = itemsSalesAmount.round();
    final t = itemsSalesRound.toRadixString(16);
    final up = t.toUpperCase();
    return up.padLeft(8, '0');
  }

  num get itemsTotalAmount {
    this.items ??= [];
    final ret = items.fold<num>(
      0.0,
      (previousValue, element) => previousValue + element.totalAmount,
    );
    return max(0.0, ret);
  }

  String get displayItemsTotalAmount {
    final value1 = max(0.0, itemsTotalAmount);
    final value2 = value1.round().decimalStyle;
    return '$value2元';
  }

  String get displayItemsCount {
    return '${normalItemsCount.decimalStyle}項';
  }

  num get itemsTaxAmount {
    // this.items ??= [];
    // return this.items.fold<num>(
    //     0.0, (previousValue, element) => previousValue + element.taxAmount);
    final value1 = itemsTotalAmount - itemsSalesAmount;
    return max(0.0, value1);
  }

  String get displayItemsTaxAmount {
    final value1 = itemsTaxAmount.round().decimalStyle;
    return '$value1元';
  }

  num get taxRate {
    return showTax ? TaxRate.TX.value : TaxRate.None.value;
  }

  // 總銷售額 (應稅+免稅)
  num get itemsSalesAmount {
    // this.items ??= [];
    // return this.items.fold<num>(
    //     0.0,
    //     (previousValue, element) =>
    //         previousValue + element.salesAmount + element.freeSalesAmount);
    // 含稅銷售額 = 總額 - 免稅銷售額
    final value1 = itemsTXWithTaxSalesAmount;
    // 銷售額
    num value2 = 0.0;
    // 含稅銷售額 > 0 需拆分
    if (value1 >= 0) {
      // 計算應稅銷售額
      value2 = (value1 / (1.0 + taxRate)) + itemsFreeSalesAmount;
    } else {
      // < 0 表示折扣，再減去免稅銷售額
      value2 = value1 + itemsFreeSalesAmount;
    }
    // 最終銷售額 (應稅+免稅)，不可 < 0
    return max(0.0, value2);
  }

  String get displayItemsSalesAmount {
    final value1 = itemsSalesAmount.round().decimalStyle;
    return '$value1元';
  }

  num get itemsFreeSalesAmount {
    items ??= [];
    return items.fold<num>(0.0,
        (previousValue, element) => previousValue + element.freeSalesAmount);
  }

  String get displayItemsFreeSalesAmount {
    final value1 = itemsFreeSalesAmount.round().decimalStyle;
    return '$value1元';
  }

  // 含稅銷售額
  num get itemsTXWithTaxSalesAmount {
    return this.itemsTotalAmount - this.itemsFreeSalesAmount;
  }

  // 應稅銷售額
  num get itemsTXSalesAmount {
    // this.items ??= [];
    // final ret = this.items.fold<num>(
    //     0.0, (previousValue, element) => previousValue + element.salesAmount);
    final value1 = itemsTXWithTaxSalesAmount / (1.0 + taxRate);
    return max(0.0, value1);
  }

  String get displayItemsTXSalesAmount {
    final value1 = itemsTXSalesAmount.round().decimalStyle;
    return '$value1元';
  }
}

// 由單價及數量計算出
// 1. 總價
// 2. 應稅銷售額
// 3. 稅額
extension ExtensionItem on Item {
  // 服務費為空值
  num get nnQuantity => quantity ?? 1;

  ItemType get itemType => type?.itemType ?? ItemType.Max;

  String get invoiceString {
    this.quantity ??= 1;
    this.unitPrice ??= 0.0;
    final value1 = itemName;
    final value2 = quantity.round();
    final value3 = unitPrice.round();
    return '$value1:$value2:$value3';
  }

  String get displayUnitPrice {
    unitPrice ??= 0.0;
    return unitPrice.round().decimalStyle;
  }

  String get leftString {
    quantity ??= 1; // 預設 1
    return '  \$$displayUnitPrice x $quantity';
  }

  String get rightString {
    // 預設應稅
    this.taxType ??= BpscmTaxType.TX.value;
    final value1 = taxType.bpscmTaxType.tail;
    return '$displayTotal元$value1';
  }

  String get displayTotal {
    return totalAmount.round().decimalStyle;
  }

  num get totalAmount {
    quantity ??= 1; // 預設 1
    unitPrice ??= 0.0;
    return quantity * unitPrice;
  }

  // num get salesAmountWithType {
  //   taxType ??= 1; // 預設應稅
  //   final taxRate = (1 == taxType) ? 0.05 : 0.0;
  //   return this.totalAmount / (1.0 + taxRate);
  // }

  // num get taxAmountWithType {
  //   return this.totalAmount - this.salesAmountWithType;
  // }

  // 免稅銷售額
  num get freeSalesAmount {
    // 預設應稅
    taxType ??= BpscmTaxType.TX.value;
    if (taxType.bpscmTaxType.isFree) {
      final _totalAmount = max(0.0, totalAmount);
      return _totalAmount;
    }
    return 0.0;
  }

  // 應稅銷售額
  num get salesAmount {
    // 預設應稅
    taxType ??= BpscmTaxType.TX.value;
    // invoice.refresh() 決定稅率
    taxRate ??= 0.0;
    if (taxType.bpscmTaxType.isTX) {
      final _totalAmount = max(0.0, totalAmount);
      // final _totalAmount = this.totalAmount;
      return (_totalAmount / (1.0 + taxRate)).roundToDouble();
    }
    return 0.0;
  }

  // 稅額 = 總額 - 銷售額
  num get taxAmount {
    // 預設應稅
    taxType ??= BpscmTaxType.TX.value;
    if (taxType.bpscmTaxType.isTX) {
      return max(0.0, totalAmount - salesAmount);
    }
    return 0.0;
  }
}

extension ExtensionReceipt on Receipt {
  // 取得一般商品 (不含額外費用、服務費)
  Iterable<Item> get normalItems {
    items ??= <Item>[];
    return items.where((element) => element.itemType.isNormal ?? false);
  }

  // 一般商品數量總和
  num get normalItemsCount {
    return normalItems.fold<num>(0, (previousValue, element) {
      return previousValue + element.nnQuantity;
    });
  }

  String get displayMasterNumber {
    if (masterNumber != null && masterNumber.isNotEmpty) {
      return masterNumber;
    }
    if (orderNumber != null && orderNumber.isNotEmpty) {
      return orderNumber;
    }
    return '';
  }

  String get target {
    switch (type.orderType) {
      case OrderType.DinnerHere:
      case OrderType.DinnerToGo:
      case OrderType.DinnerDelivery:
        return '顧客';
      case OrderType.RetailToGo:
        return '取貨';
      case OrderType.RetailDelivery:
        return '收件人';
      case OrderType.RetailInStore:
        return '取貨';
      default:
        return '';
    }
  }

  bool get needAddress {
    switch (type.orderType) {
      case OrderType.RetailToGo:
        return false;
      default:
        return true;
    }
  }

  bool get isDelivery {
    if (OrderType.DinnerDelivery.index == type) {
      return true;
    }
    if (OrderType.RetailDelivery.index == type) {
      return true;
    }
    return false;
  }

  String get displayBuyerMemo {
    buyerMemo ??= '';
    return buyerMemo.replaceAll('\n', "").trim();
  }

  String get displaySellerMemo {
    sellerMemo ??= '';
    return sellerMemo.trim();
  }

  String get displayCategory {
    switch (storeType) {
      case StoreType.Dinner:
        return '用餐';
      case StoreType.Retail:
        return '取貨';
      default:
        return '';
    }
  }

  StoreType get storeType => type.orderType.storeType;

  String get orderNumberSimply {
    orderNumber ??= '';
    return orderNumber.takeLast(3);
  }

  String get displayType {
    return type.orderType.getNameWithSource(source.orderSource);
  }

  String get displayStatus {
    // 內用 / 外帶 都要保留桌號
    // switch (this.type.orderType) {
    //   case OrderType.DinnerHere:
    //   case OrderType.DinnerToGo:
    //     return this.seat ?? '';
    //   default:
    // }
    return status.orderStatus.name;
  }

  String get displayPaymentFee {
    paymentFee ??= 0.0;
    return paymentFee.decimalStyle;
  }

  String get displayInvoiceStatus => invoiceStatus.invoiceStatus.name;

  String get displayCreatedAt => createdAt?.y_MM_ddHHmm ?? '';

  String get displayCheckoutAt => checkoutAt?.MM_ddHHmm ?? '';

  String get displayPrintAt => printAt?.MM_ddHHmm ?? '';

  num get itemCount {
    items ??= <Item>[];
    return items.length;
  }

  // TODO: remove this.
  // 展開數量
  num get flatItemCount => normalItemsCount;

  // 全部商品數量
  // 內含:
  // 1. 額外費用
  // 2. 服務費 (getFee)
  // 外加:
  // 1. 運費 (OrderShipping)
  // 2. 金流手續費 (OrderPaymentInfo)
  // 3. 現場折價 (getDiscount)
  // num get buyItemCount {
  //   this.rItems ??= [];
  //   return this.rItems.length;
  // }

  String get displayOtherFee {
    otherFee ??= 0.0;
    return otherFee.round().decimalStyle;
  }

  String get displayDeliveryFee {
    deliveryFee ??= 0.0;
    return deliveryFee.round().decimalStyle;
  }

  String get displayServiceFee {
    serviceFee ??= 0.0;
    return serviceFee.round().decimalStyle;
  }

  String get displayPaid {
    paid ??= 0.0;
    return paid.round().decimalStyle;
  }

  String get displayChange {
    change ??= 0.0;
    return change.round().decimalStyle;
  }

  String get displayTotal {
    total ??= 0.0;
    return total.round().decimalStyle;
  }

  String get displayDiscount {
    discount ??= 0.0;
    return discount.round().decimalStyle;
  }

  String get displaySubtotal {
    subtotal ??= 0.0;
    return subtotal.round().decimalStyle;
  }

  String get displayMealAt => mealAt?.y_MM_ddHHmm ?? '';

  String get displayPaymentStatus => paymentStatus.paymentStatus.name;
}

extension ExtensionMember on Member {
  String get nicknameAndName {
    if (nicknameStore != null && nicknameStore.isNotEmpty) {
      return '($nicknameStore)$name';
    }
    return name ?? '';
  }

  num get allCouponCount {
    return memberCouponCount.fold(
      0,
      (previousValue, element) => previousValue + (element.count ?? 0),
    );
  }

  num getCouponCount(StoreType storeType, OrderSource orderSource) {
    memberCouponCount ??= [];
    final it = memberCouponCount.firstWhere(
      (element) {
        final coupon = Coupon.fromRawJson(element.toRawJson());
        return coupon.storeType == storeType &&
            coupon.orderSource == orderSource;
      },
      orElse: () => MemberCouponCount(),
    );
    return it?.count ?? 0;
  }
}

extension ExtensionCoupon on Coupon {
  set promotionValue(num value) {
    value = (value ?? 0).round().abs();
    promotionType ??= PromotionType.Max.index;
    switch (promotionType.promotionType) {
      case PromotionType.Off:
      case PromotionType.Discount:
        discount = -value;
        break;
      case PromotionType.Gift:
      case PromotionType.Upgrade:
        extraPrice = value;
        break;
      default:
    }
  }

  num get promotionValue {
    switch (promotionType.promotionType) {
      case PromotionType.Off:
      case PromotionType.Discount:
        return -absDiscount;
        break;
      case PromotionType.Gift:
      case PromotionType.Upgrade:
        return absExtraPrice;
      default:
    }
  }

  String get displayPromotionTypeHint {
    promotionType ??= PromotionType.Max.index;
    switch (promotionType.promotionType) {
      case PromotionType.Off: // 打折
        return '(1~99)折';
      case PromotionType.Discount: // 減價
      case PromotionType.Gift: // 贈送
      case PromotionType.Upgrade: // 升級
        return '金額';
      default:
        return '金額';
    }
  }

  String get displayPromotionValue {
    promotionType ??= PromotionType.Max.index;
    switch (promotionType.promotionType) {
      case PromotionType.Discount: // 減價
        return '-$absDiscount';
      case PromotionType.Off: // 打折
        return '$absDiscount';
      case PromotionType.Gift: // 贈送
      case PromotionType.Upgrade: // 升級
        return '$absExtraPrice';
      default:
        return '';
    }
  }

  String get displayTitle {
    switch (promotionType?.promotionType ?? PromotionType.Max) {
      case PromotionType.Off:
        return '$title($_offString折)';
      case PromotionType.Discount:
        return '$title($promotionValue)';
      case PromotionType.Gift:
      case PromotionType.Upgrade:
        return '$title(+$promotionValue)';
      default:
        return '';
    }
  }

  String get _offString {
    if (absDiscount % 10 > 0) {
      return '$absDiscount';
    }
    return '${(absDiscount * 0.1).round()}';
  }

  num get absDiscount {
    discount = (discount ?? 0).round();
    return discount.abs();
  }

  num get absExtraPrice {
    extraPrice = (extraPrice ?? 0).round();
    return extraPrice.abs();
  }

  // 取得減價的絕對值
  num getMemberDiscount(num value) {
    value = max(0, value ?? 0);
    switch (promotionType.promotionType) {
      case PromotionType.Off: // 打折
        return lerpDouble(value, 0, floatingDiscount ?? 1);
      case PromotionType.Discount: // 折價
        return (discount ?? 0).clamp(0, value);
      default:
        return 0;
    }
  }

  // 取得加價的絕對值
  num get memberCouponExtraPrice {
    switch (promotionType.promotionType) {
      case PromotionType.Gift: // 贈送
      case PromotionType.Upgrade: // 升級
        return max(extraPrice ?? 0, 0);
      default:
        return 0;
    }
  }

  // 打8折 = 80 -> 0.8
  num get floatingDiscount => (discount ?? 0).clamp(0, 100) * 0.01;

  bool get isAvailable => message == null || message.isEmpty;

  String get message {
    if (isExpired) {
      return '優惠券逾期';
    }
    if (!hasRemaining) {
      return '優惠券用罄';
    }
    if (status != null && !status.couponStatus.isAvailable) {
      return status.couponStatus.name;
    }
    return '';
  }

  ///
  /// 數量
  ///
  bool get hasRemaining {
    if (lastCount != null && lastCount <= 0) {
      return false;
    }
    return true;
  }

  ///
  /// 逾期
  ///
  bool get isExpired {
    final date = lastUseDate ?? expiryDate;
    if (date == null) {
      return false;
    }
    final now = DateTime.now();
    return date.localAt.millisecondsSinceEpoch < now.millisecondsSinceEpoch;
  }

  String get displayLastUseDate {
    final date = lastUseDate ?? expiryDate;
    return date != null ? '${date.localAt.y_MM_ddHHmmss}止' : '-';
  }

  String get displayType {
    return '${storeType.name}${isOnline.lineType.name}券';
  }

  bool get isLimited => lastCount != null && lastCount >= 0;

  /// kind
  /// 1: 餐飲
  /// 2: 零售
  StoreType get storeType {
    if (1 == kind) {
      return StoreType.Dinner;
    }
    if (2 == kind) {
      return StoreType.Retail;
    }
    return StoreType.Max;
  }

  OrderSource get orderSource => isOnline.orderSource;
  // OrderSource get orderSource {
  //   if (0 == isOnline) {
  //     return OrderSource.App;
  //   }
  //   if (1 == isOnline) {
  //     return OrderSource.Line;
  //   }
  //   return OrderSource.Max;
  // }
}

extension ExtensionPagination on Pagination {
  bool get needToLoadMore => !isLastPage;

  bool get isLastPage {
    final curr = currentPage ?? 0;
    final last = lastPage ?? double.maxFinite.round();
    return curr >= last;
  }

  num get nextPage {
    final curr = currentPage ?? 0;
    return curr + 1;
  }

  void resetLastPage() {
    lastPage = double.maxFinite.round();
  }

  // 使目前頁為最後一頁
  void setLastPage() {
    lastPage = currentPage;
  }
}

extension ExtensionSettingLabel on SettingLabel {
  num getMaxPrintCount(Iterable<num> ids) {
    return categorySettings.entries.where(
      (element) {
        return ids.contains(num.tryParse(element.key));
      },
    ).fold(
      1,
      (previousValue, element) {
        return max(previousValue, element.value.printCount ?? 1);
      },
    );
  }

  // flat
  Map<String, CategorySetting> get categorySettings {
    nnOther.categorySettings ??= <String, CategorySetting>{};
    return nnOther.categorySettings;
  }

  Other get nnOther {
    other ??= Other();
    return other;
  }

  SettingLabel clone() => SettingLabel.fromRawJson(toRawJson());

  num get uuid {
    if (macAddressOnly != null && macAddressOnly.isNotEmpty) {
      return macAddressOnly.hashCode;
    }
    if (ip != null && ip.isNotEmpty) {
      return ip.hashCode;
    }
    return id;
  }

  // 取得一個分類是否包含在設定內
  bool containsCategory(num categoryId) {
    categoryIds ??= <num>[];
    return categoryIds.contains(categoryId);
  }

  // 取得一個分類是否包含在設定內
  bool containsCategories(Iterable<num> value) {
    return value.any((element) => containsCategory(element));
  }

  // 嘗試設定一個分類 Toggle
  void setHasCategory(num categoryId, bool b) {
    if (b) {
      _addCategoryId(categoryId);
    } else {
      _removeCategoryId(categoryId);
    }
  }

  void _addCategoryId(num id) {
    categoryIds ??= <num>[];
    categoryIds.add(id);
    categoryIds = categoryIds.toSet().toList();
  }

  void _removeCategoryId(num id) {
    categoryIds ??= <num>[];
    categoryIds.remove(id);
  }

  String get host {
    final list = ip?.split(':') ?? <String>[];
    return list.isNotEmpty ? list.first : '';
  }

  num get port {
    final port1 = _afterColon(ip);
    if (port1 != null && port1.isNotEmpty) {
      return num.tryParse(port1);
    }
    final port2 = _afterColon(macAddressOnly);
    return num.tryParse(port2);
  }

  static String _afterColon(String input) {
    final list = input?.split(':') ?? <String>[];
    return list.length > 1 ? list.elementAt(1) : '';
  }

  String get type {
    final list = macAddress?.split('@') ?? <String>[];
    final lhs = list != null && list.isNotEmpty ? list.first : '';
    return lhs.split(':').first;
  }

  // 1: 58mm (預設)
  // 2: 80mm
  PrinterPaperSize get printerPaperSize {
    final list = macAddress?.split('@') ?? <String>[];
    final lhs = list != null && list.isNotEmpty ? list.first : '';
    final list2 = lhs.split(':');
    final paperSize = list2.length > 1 ? list2.elementAt(1) : '1';
    return num.tryParse(paperSize)?.printerPaperSize ?? PrinterPaperSize.None;
  }

  set printerPaperSize(PrinterPaperSize value) {
    macAddress = '$type:${value.index}@$macAddressOnly';
  }

  String get macAddressOnly {
    final list = macAddress?.split('@') ?? <String>[];
    return (list != null && list.length > 1) ? list.elementAt(1) : '';
  }
}

extension ExtensionRevenue on Revenue {
  OrderType get orderType {
    type ??= -1;
    return type.orderType;
  }

  OrderSource get orderSource {
    source ??= -1;
    return source.orderSource;
  }

  String get displayType {
    return orderType.getNameWithSource(orderSource);
  }

  Color get displayColor {
    return orderType.getColorWithSource(orderSource);
  }

  num get orderSerialNumber {
    return num.tryParse(orderSerial) ?? 0;
  }

  String get orderSerial {
    orderNumber ??= '';
    return orderNumber.takeLast(6).padLeft(6, '0');
  }
}

extension ExtensionOrderTotal on OrderTotal {
  // 取得餐飲數字
  // time: 0(now), 1(next)
  // online: 0(app), 1(line), 2(all)
  // type: 0(in), 1(togo), 2(delivery), 3(takeout)
  // 0: 內用
  // 1: 自取
  // 2: 外送
  // 3: 外帶
  num getDinerCount(OrdersBusinessHoursMode time, num index) {
    if (OrdersBusinessHoursMode.Current == time) {
      switch (index) {
        case 0:
          return _dinerCurrentHere;
        case 1:
          return _dinerCurrentLineTogo;
        case 2:
          return _dinerCurrentDelivery;
        case 3:
          return _dinerCurrentAppTogo;
        default:
          return 0;
      }
    }
    if (OrdersBusinessHoursMode.Other == time) {
      switch (index) {
        case 0:
          return _dinerNextHere;
        case 1:
          return _dinerNextLineTogo;
        case 2:
          return _dinerNextDelivery;
        case 3:
          return _dinerNextAppTogo;
        default:
          return 0;
      }
    }
    return 0;
  }

  ///
  /// 餐飲本時段內用
  ///
  num get _dinerCurrentHere {
    final count1 = this.dinerOnline?.now?.dinerIn ?? 0;
    final count2 = this.diner?.now?.dinerIn ?? 0;
    return count1 + count2;
  }

  ///
  /// 餐飲本時段外送
  ///
  num get _dinerCurrentDelivery {
    final count1 = this.dinerOnline?.now?.delivery ?? 0;
    final count2 = this.diner?.now?.delivery ?? 0;
    return count1 + count2;
  }

  ///
  /// 餐飲本時段自取
  ///
  num get _dinerCurrentLineTogo {
    final count1 = this.dinerOnline?.now?.toGo ?? 0;
    // final count2 = this.diner?.now?.toGo ?? 0;
    return count1;
  }

  ///
  /// 餐飲本時段外帶
  ///
  num get _dinerCurrentAppTogo {
    // final count1 = this.dinerOnline?.now?.toGo ?? 0;
    final count2 = this.diner?.now?.toGo ?? 0;
    return count2;
  }

  ///
  /// 餐飲下時段內用
  ///
  num get _dinerNextHere {
    final count1 = this.dinerOnline?.next?.dinerIn ?? 0;
    final count2 = this.diner?.next?.dinerIn ?? 0;
    return count1 + count2;
  }

  ///
  /// 餐飲下時段外送
  ///
  num get _dinerNextDelivery {
    final count1 = this.dinerOnline?.next?.delivery ?? 0;
    final count2 = this.diner?.next?.delivery ?? 0;
    return count1 + count2;
  }

  ///
  /// 餐飲下時段自取
  ///
  num get _dinerNextLineTogo {
    final count1 = this.dinerOnline?.next?.toGo ?? 0;
    // final count2 = this.diner?.next?.toGo ?? 0;
    return count1;
  }

  ///
  /// 餐飲下時段外帶
  ///
  num get _dinerNextAppTogo {
    // final count1 = this.dinerOnline?.next?.toGo ?? 0;
    final count2 = this.diner?.next?.toGo ?? 0;
    return count2;
  }

  // TODO: change args
  // 0：自取
  // 1：宅配
  // 2：超取
  num getShopCount(num index) {
    switch (index) {
      case 0:
        return this.shopInstore;
      case 1:
        return this.shopDelivery;
      case 2:
        return 0; // TODO:
      default:
        return 0;
    }
  }

  num get shopInstore {
    final count1 = this.shop?.now?.instore ?? 0;
    final count2 = this.shop?.next?.instore ?? 0;
    final count3 = this.shopOnline?.now?.instore ?? 0;
    final count4 = this.shopOnline?.next?.instore ?? 0;
    return count1 + count2 + count3 + count4;
  }

  num get shopDelivery {
    final count1 = this.shop?.now?.delivery ?? 0;
    final count2 = this.shop?.next?.delivery ?? 0;
    final count3 = this.shopOnline?.now?.delivery ?? 0;
    final count4 = this.shopOnline?.next?.delivery ?? 0;
    return count1 + count2 + count3 + count4;
  }

  num get dinerCurrentTotal {
    final app = this.total?.dinerNow ?? 0;
    final online = this.total?.dinerOnlineNow ?? 0;
    return app + online;
  }

  num get dinerNextTotal {
    final app = this.total?.dinerNext ?? 0;
    final online = this.total?.dinerOnlineNext ?? 0;
    return app + online;
  }

  num get shopTotal {
    final count1 = this.total?.shopNow ?? 0;
    final count2 = this.total?.shopNext ?? 0;
    final count3 = this.total?.shopOnlineNow ?? 0;
    final count4 = this.total?.shopOnlineNext ?? 0;
    return count1 + count2 + count3 + count4;
  }

  num get dinerTotal => dinerCurrentTotal + dinerNextTotal;
}

extension ExtensionTable on Table {
  TableReq asTableReq() {
    return TableReq(
      name: name,
      parentId: parentId,
      sort: sort,
    );
  }
}

extension ExtensionTableReq on TableReq {
  Table asTable({
    num id,
    List<Table> child,
  }) {
    return Table(
      id: id,
      child: child ?? <Table>[],
      name: name,
      parentId: parentId,
      sort: sort,
    );
  }
}

extension ProductInfoX on ProductInfo {
  String get uniqueKey => '$categoryId.$productId';
  // product id 可能重複，使用 product id + category id 取 hashcode
  num get uniqueId => uniqueKey.hashCode;
  num get id => uniqueId;
}
