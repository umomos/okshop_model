// To parse this JSON data, do
//
//     final additionProduct = additionProductFromJson(jsonString);

import 'dart:convert';

import 'package:objectbox/objectbox.dart';

// @Entity()
class AdditionProduct {
  AdditionProduct({
    this.additionCategoryId,
    this.id,
    this.kind,
    this.name,
    this.price,
    this.sort,
  });

  @Property(type: PropertyType.int)
  num additionCategoryId;
  @Id(assignable: true)
  int id;
  @Property(type: PropertyType.int)
  num kind;
  String name;
  @Property(type: PropertyType.float)
  num price;
  @Property(type: PropertyType.int)
  num sort;

  AdditionProduct copyWith({
    num additionCategoryId,
    num id,
    num kind,
    String name,
    num price,
    num sort,
  }) =>
      AdditionProduct(
        additionCategoryId: additionCategoryId ?? this.additionCategoryId,
        id: id ?? this.id,
        kind: kind ?? this.kind,
        name: name ?? this.name,
        price: price ?? this.price,
        sort: sort ?? this.sort,
      );

  factory AdditionProduct.fromRawJson(String str) =>
      AdditionProduct.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory AdditionProduct.fromJson(Map<String, dynamic> json) =>
      AdditionProduct(
        additionCategoryId: json["addition_category_id"],
        id: json["id"],
        kind: json["kind"],
        name: json["name"],
        price: json["price"],
        sort: json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "addition_category_id": additionCategoryId,
        "id": id,
        "kind": kind,
        "name": name,
        "price": price,
        "sort": sort,
      };
}
