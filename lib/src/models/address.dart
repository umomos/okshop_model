// To parse this JSON data, do
//
//     final address = addressFromJson(jsonString);

import 'dart:convert';

class Address {
  Address({
    this.address,
    this.cityId,
    this.cityareaId,
    this.id,
    this.isDefaut,
    this.name,
    this.phone,
    this.postcode,
    this.type,
  });

  String address;
  num cityId;
  num cityareaId;
  num id;
  num isDefaut;
  String name;
  String phone;
  String postcode;
  num type;

  Address copyWith({
    String address,
    num cityId,
    num cityareaId,
    num id,
    num isDefaut,
    String name,
    String phone,
    String postcode,
    num type,
  }) =>
      Address(
        address: address ?? this.address,
        cityId: cityId ?? this.cityId,
        cityareaId: cityareaId ?? this.cityareaId,
        id: id ?? this.id,
        isDefaut: isDefaut ?? this.isDefaut,
        name: name ?? this.name,
        phone: phone ?? this.phone,
        postcode: postcode ?? this.postcode,
        type: type ?? this.type,
      );

  factory Address.fromRawJson(String str) => Address.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        address: json["address"] == null ? null : json["address"],
        cityId: json["city_id"] == null ? null : json["city_id"],
        cityareaId: json["cityarea_id"] == null ? null : json["cityarea_id"],
        id: json["id"] == null ? null : json["id"],
        isDefaut: json["is_defaut"] == null ? null : json["is_defaut"],
        name: json["name"] == null ? null : json["name"],
        phone: json["phone"] == null ? null : json["phone"],
        postcode: json["postcode"] == null ? null : json["postcode"],
        type: json["type"] == null ? null : json["type"],
      );

  Map<String, dynamic> toJson() => {
        "address": address == null ? null : address,
        "city_id": cityId == null ? null : cityId,
        "cityarea_id": cityareaId == null ? null : cityareaId,
        "id": id == null ? null : id,
        "is_defaut": isDefaut == null ? null : isDefaut,
        "name": name == null ? null : name,
        "phone": phone == null ? null : phone,
        "postcode": postcode == null ? null : postcode,
        "type": type == null ? null : type,
      };
}
