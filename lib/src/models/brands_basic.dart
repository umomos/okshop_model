// To parse this JSON data, do
//
//     final brandsBasic = brandsBasicFromJson(jsonString);

import 'dart:convert';

class BrandsBasic {
  BrandsBasic({
    this.address,
    this.cityId,
    this.cityareaId,
    this.customUrl,
    this.email,
    this.facebook,
    this.instagram,
    this.phone,
    this.taxId,
    this.taxName,
    this.website,
    this.youtube,
  });

  String address;
  num cityId;
  num cityareaId;
  List<CustomUrl> customUrl;
  String email;
  String facebook;
  String instagram;
  String phone;
  String taxId;
  String taxName;
  String website;
  String youtube;

  BrandsBasic copyWith({
    String address,
    num cityId,
    num cityareaId,
    List<CustomUrl> customUrl,
    String email,
    String facebook,
    String instagram,
    String phone,
    String taxId,
    String taxName,
    String website,
    String youtube,
  }) =>
      BrandsBasic(
        address: address ?? this.address,
        cityId: cityId ?? this.cityId,
        cityareaId: cityareaId ?? this.cityareaId,
        customUrl: customUrl ?? this.customUrl,
        email: email ?? this.email,
        facebook: facebook ?? this.facebook,
        instagram: instagram ?? this.instagram,
        phone: phone ?? this.phone,
        taxId: taxId ?? this.taxId,
        taxName: taxName ?? this.taxName,
        website: website ?? this.website,
        youtube: youtube ?? this.youtube,
      );

  factory BrandsBasic.fromRawJson(String str) =>
      BrandsBasic.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory BrandsBasic.fromJson(Map<String, dynamic> json) => BrandsBasic(
        address: json["address"],
        cityId: json["city_id"],
        cityareaId: json["cityarea_id"],
        customUrl: json["custom_url"] == null
            ? null
            : List<CustomUrl>.from(
                json["custom_url"].map((x) => CustomUrl.fromJson(x))),
        email: json["email"],
        facebook: json["facebook"],
        instagram: json["instagram"],
        phone: json["phone"],
        taxId: json["tax_id"],
        taxName: json["tax_name"],
        website: json["website"],
        youtube: json["youtube"],
      );

  Map<String, dynamic> toJson() => {
        "address": address,
        "city_id": cityId,
        "cityarea_id": cityareaId,
        "custom_url": customUrl == null
            ? null
            : List<dynamic>.from(customUrl.map((x) => x.toJson())),
        "email": email,
        "facebook": facebook,
        "instagram": instagram,
        "phone": phone,
        "tax_id": taxId,
        "tax_name": taxName,
        "website": website,
        "youtube": youtube,
      };
}

class CustomUrl {
  CustomUrl({
    this.name,
    this.url,
  });

  String name;
  String url;

  CustomUrl copyWith({
    String name,
    String url,
  }) =>
      CustomUrl(
        name: name ?? this.name,
        url: url ?? this.url,
      );

  factory CustomUrl.fromRawJson(String str) =>
      CustomUrl.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CustomUrl.fromJson(Map<String, dynamic> json) => CustomUrl(
        name: json["name"],
        url: json["url"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "url": url,
      };
}
