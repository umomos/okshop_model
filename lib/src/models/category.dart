// To parse this JSON data, do
//
//     final category = categoryFromJson(jsonString);

import 'dart:convert';

import 'package:objectbox/objectbox.dart';

// @Entity()
class Category {
  Category({
    this.id,
    this.kind,
    this.name,
    this.sort,
  });

  @Id(assignable: true)
  int id;
  @Property(type: PropertyType.int)
  num kind;
  String name;
  @Property(type: PropertyType.int)
  num sort;

  Category copyWith({
    num id,
    num kind,
    String name,
    num sort,
  }) =>
      Category(
        id: id ?? this.id,
        kind: kind ?? this.kind,
        name: name ?? this.name,
        sort: sort ?? this.sort,
      );

  factory Category.fromRawJson(String str) =>
      Category.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json["id"],
        kind: json["kind"],
        name: json["name"],
        sort: json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "kind": kind,
        "name": name,
        "sort": sort,
      };
}
