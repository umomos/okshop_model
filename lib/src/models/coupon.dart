// To parse this JSON data, do
//
//     final coupon = couponFromJson(jsonString);

import 'dart:convert';

class Coupon {
  Coupon({
    this.description,
    this.couponId,
    this.expiryDate,
    this.id,
    this.imageUrl,
    this.isOnline,
    this.kind,
    this.lastCount,
    this.lastUseDate,
    this.promotionType,
    this.quantityLimit,
    this.status,
    this.title,
    this.usagePeriod,
    this.extraPrice,
    this.minPrice,
    this.discount,
    this.sort,
  });

  String description;
  num couponId;
  String expiryDate;
  num id;
  String imageUrl;
  num isOnline;
  num kind;
  num lastCount;
  String lastUseDate;
  num promotionType;
  num quantityLimit;
  num status;
  String title;
  num usagePeriod;
  num extraPrice;
  num minPrice;
  num discount;
  num sort;

  Coupon copyWith({
    String description,
    num couponId,
    String expiryDate,
    num id,
    String imageUrl,
    num isOnline,
    num kind,
    num lastCount,
    String lastUseDate,
    num promotionType,
    num quantityLimit,
    num status,
    String title,
    num usagePeriod,
    num extraPrice,
    num minPrice,
    num discount,
    num sort,
  }) =>
      Coupon(
        description: description ?? this.description,
        couponId: couponId ?? this.couponId,
        expiryDate: expiryDate ?? this.expiryDate,
        id: id ?? this.id,
        imageUrl: imageUrl ?? this.imageUrl,
        isOnline: isOnline ?? this.isOnline,
        kind: kind ?? this.kind,
        lastCount: lastCount ?? this.lastCount,
        lastUseDate: lastUseDate ?? this.lastUseDate,
        promotionType: promotionType ?? this.promotionType,
        quantityLimit: quantityLimit ?? this.quantityLimit,
        status: status ?? this.status,
        title: title ?? this.title,
        usagePeriod: usagePeriod ?? this.usagePeriod,
        extraPrice: extraPrice ?? this.extraPrice,
        minPrice: minPrice ?? this.minPrice,
        discount: discount ?? this.discount,
        sort: sort ?? this.sort,
      );

  factory Coupon.fromRawJson(String str) => Coupon.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Coupon.fromJson(Map<String, dynamic> json) => Coupon(
        description: json["description"] == null ? null : json["description"],
        couponId: json["coupon_id"] == null ? null : json["coupon_id"],
        expiryDate: json["expiry_date"] == null ? null : json["expiry_date"],
        id: json["id"] == null ? null : json["id"],
        imageUrl: json["image_url"] == null ? null : json["image_url"],
        isOnline: json["is_online"] == null ? null : json["is_online"],
        kind: json["kind"] == null ? null : json["kind"],
        lastCount: json["last_count"] == null ? null : json["last_count"],
        lastUseDate:
            json["last_use_date"] == null ? null : json["last_use_date"],
        promotionType:
            json["promotion_type"] == null ? null : json["promotion_type"],
        quantityLimit:
            json["quantity_limit"] == null ? null : json["quantity_limit"],
        status: json["status"] == null ? null : json["status"],
        title: json["title"] == null ? null : json["title"],
        usagePeriod: json["usage_period"] == null ? null : json["usage_period"],
        extraPrice: json["extra_price"] == null ? null : json["extra_price"],
        minPrice: json["min_price"] == null ? null : json["min_price"],
        discount: json["discount"] == null ? null : json["discount"],
        sort: json["sort"] == null ? null : json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "description": description == null ? null : description,
        "coupon_id": couponId == null ? null : couponId,
        "expiry_date": expiryDate == null ? null : expiryDate,
        "id": id == null ? null : id,
        "image_url": imageUrl == null ? null : imageUrl,
        "is_online": isOnline == null ? null : isOnline,
        "kind": kind == null ? null : kind,
        "last_count": lastCount == null ? null : lastCount,
        "last_use_date": lastUseDate == null ? null : lastUseDate,
        "promotion_type": promotionType == null ? null : promotionType,
        "quantity_limit": quantityLimit == null ? null : quantityLimit,
        "status": status == null ? null : status,
        "title": title == null ? null : title,
        "usage_period": usagePeriod == null ? null : usagePeriod,
        "extra_price": extraPrice == null ? null : extraPrice,
        "min_price": minPrice == null ? null : minPrice,
        "discount": discount == null ? null : discount,
        "sort": sort == null ? null : sort,
      };
}
