// To parse this JSON data, do
//
//     final invoice = invoiceFromJson(jsonString);

import 'dart:convert';

import 'item.dart';

class Invoice {
  Invoice({
    this.storeName,
    this.buyerName,
    this.printMark,
    this.taxType,
    this.dateTime,
    this.invoiceNumber,
    this.randomNumber,
    this.seller,
    this.buyer,
    this.items,
  });

  String storeName;
  String buyerName;
  num printMark;
  num taxType;
  DateTime dateTime;
  String invoiceNumber;
  String randomNumber;
  String seller;
  String buyer;
  List<Item> items;

  Invoice copyWith({
    String storeName,
    String buyerName,
    num printMark,
    num taxType,
    DateTime dateTime,
    String invoiceNumber,
    String randomNumber,
    String seller,
    String buyer,
    List<Item> items,
  }) =>
      Invoice(
        storeName: storeName ?? this.storeName,
        buyerName: buyerName ?? this.buyerName,
        printMark: printMark ?? this.printMark,
        taxType: taxType ?? this.taxType,
        dateTime: dateTime ?? this.dateTime,
        invoiceNumber: invoiceNumber ?? this.invoiceNumber,
        randomNumber: randomNumber ?? this.randomNumber,
        seller: seller ?? this.seller,
        buyer: buyer ?? this.buyer,
        items: items ?? this.items,
      );

  factory Invoice.fromRawJson(String str) => Invoice.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Invoice.fromJson(Map<String, dynamic> json) => Invoice(
        storeName: json["storeName"] == null ? null : json["storeName"],
        buyerName: json["buyer_name"] == null ? null : json["buyer_name"],
        printMark: json["print_mark"] == null ? null : json["print_mark"],
        taxType: json["tax_type"] == null ? null : json["tax_type"],
        dateTime: json["date_time"] == null
            ? null
            : DateTime.parse(json["date_time"]),
        invoiceNumber:
            json["invoice_number"] == null ? null : json["invoice_number"],
        randomNumber:
            json["random_number"] == null ? null : json["random_number"],
        seller: json["seller"] == null ? null : json["seller"],
        buyer: json["buyer"] == null ? null : json["buyer"],
        items: json["items"] == null
            ? null
            : List<Item>.from(json["items"].map((x) => Item.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "storeName": storeName == null ? null : storeName,
        "buyer_name": buyerName == null ? null : buyerName,
        "print_mark": printMark == null ? null : printMark,
        "tax_type": taxType == null ? null : taxType,
        "date_time": dateTime == null ? null : dateTime.toIso8601String(),
        "invoice_number": invoiceNumber == null ? null : invoiceNumber,
        "random_number": randomNumber == null ? null : randomNumber,
        "seller": seller == null ? null : seller,
        "buyer": buyer == null ? null : buyer,
        "items": items == null
            ? null
            : List<dynamic>.from(items.map((x) => x.toJson())),
      };
}

// class Item {
//   Item({
//     this.itemName,
//     this.quantity,
//     this.unitPrice,
//     this.taxRate,
//     this.taxType,
//     this.comment,
//     this.productId,
//   });

//   String itemName;
//   num quantity;
//   num unitPrice;
//   num taxRate;
//   num taxType;
//   String comment;
//   num productId;

//   Item copyWith({
//     String itemName,
//     num quantity,
//     num unitPrice,
//     num taxRate,
//     num taxType,
//     String comment,
//     num productId,
//   }) =>
//       Item(
//         itemName: itemName ?? this.itemName,
//         quantity: quantity ?? this.quantity,
//         unitPrice: unitPrice ?? this.unitPrice,
//         taxRate: taxRate ?? this.taxRate,
//         taxType: taxType ?? this.taxType,
//         comment: comment ?? this.comment,
//         productId: productId ?? this.productId,
//       );

//   factory Item.fromRawJson(String str) => Item.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory Item.fromJson(Map<String, dynamic> json) => Item(
//         itemName: json["item_name"] == null ? null : json["item_name"],
//         quantity: json["quantity"] == null ? null : json["quantity"],
//         unitPrice: json["unit_price"] == null ? null : json["unit_price"],
//         taxRate: json["taxRate"] == null ? null : json["taxRate"].toDouble(),
//         taxType: json["tax_type"] == null ? null : json["tax_type"],
//         comment: json["comment"] == null ? null : json["comment"],
//         productId: json["product_id"] == null ? null : json["product_id"],
//       );

//   Map<String, dynamic> toJson() => {
//         "item_name": itemName == null ? null : itemName,
//         "quantity": quantity == null ? null : quantity,
//         "unit_price": unitPrice == null ? null : unitPrice,
//         "taxRate": taxRate == null ? null : taxRate,
//         "tax_type": taxType == null ? null : taxType,
//         "comment": comment == null ? null : comment,
//         "product_id": productId == null ? null : productId,
//       };
// }
