// To parse this JSON data, do
//
//     final item = itemFromJson(jsonString);

import 'dart:convert';

class Item {
  Item({
    this.itemName,
    this.quantity,
    this.unitPrice,
    this.taxRate,
    this.taxType,
    this.comment,
    this.productId,
    this.type,
  });

  String itemName;
  num quantity;
  num unitPrice;
  double taxRate;
  num taxType;
  String comment;
  num productId;
  num type;

  Item copyWith({
    String itemName,
    num quantity,
    num unitPrice,
    double taxRate,
    num taxType,
    String comment,
    num productId,
    num type,
  }) =>
      Item(
        itemName: itemName ?? this.itemName,
        quantity: quantity ?? this.quantity,
        unitPrice: unitPrice ?? this.unitPrice,
        taxRate: taxRate ?? this.taxRate,
        taxType: taxType ?? this.taxType,
        comment: comment ?? this.comment,
        productId: productId ?? this.productId,
        type: type ?? this.type,
      );

  factory Item.fromRawJson(String str) => Item.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Item.fromJson(Map<String, dynamic> json) => Item(
        itemName: json["item_name"] == null ? null : json["item_name"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        unitPrice: json["unit_price"] == null ? null : json["unit_price"],
        taxRate: json["taxRate"] == null ? null : json["taxRate"].toDouble(),
        taxType: json["tax_type"] == null ? null : json["tax_type"],
        comment: json["comment"] == null ? null : json["comment"],
        productId: json["product_id"] == null ? null : json["product_id"],
        type: json["type"] == null ? null : json["type"],
      );

  Map<String, dynamic> toJson() => {
        "item_name": itemName == null ? null : itemName,
        "quantity": quantity == null ? null : quantity,
        "unit_price": unitPrice == null ? null : unitPrice,
        "taxRate": taxRate == null ? null : taxRate,
        "tax_type": taxType == null ? null : taxType,
        "comment": comment == null ? null : comment,
        "product_id": productId == null ? null : productId,
        "type": type == null ? null : type,
      };
}
