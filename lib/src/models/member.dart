// To parse this JSON data, do
//
//     final member = memberFromJson(jsonString);

import 'dart:convert';

import 'package:objectbox/objectbox.dart';

import 'order_summary.dart';

@Entity()
class Member {
  Member({
    this.avatar,
    this.id,
    this.isVip,
    this.lastOrder,
    this.memberCouponCount,
    this.memo,
    this.mobilePhone,
    this.name,
    this.orderCount,
    this.points,
    this.status,
    this.nicknameStore,
  });

  String avatar;
  @Id(assignable: true)
  int id;
  @Property(type: PropertyType.int)
  num isVip;
  LastOrder lastOrder;
  List<MemberCouponCount> memberCouponCount;
  String memo;
  String mobilePhone;
  String name;
  @Property(type: PropertyType.int)
  num orderCount;
  @Property(type: PropertyType.int)
  num points;
  @Property(type: PropertyType.int)
  num status;
  String nicknameStore;
  String rawJson;
  @Property(type: PropertyType.int)
  num timestamp;
  @Backlink()
  final orders = ToMany<OrderSummary>();

  Member copyWith({
    String avatar,
    num id,
    num isVip,
    LastOrder lastOrder,
    List<MemberCouponCount> memberCouponCount,
    String memo,
    String mobilePhone,
    String name,
    num orderCount,
    num points,
    num status,
    String nicknameStore,
  }) =>
      Member(
        avatar: avatar ?? this.avatar,
        id: id ?? this.id,
        isVip: isVip ?? this.isVip,
        lastOrder: lastOrder ?? this.lastOrder,
        memberCouponCount: memberCouponCount ?? this.memberCouponCount,
        memo: memo ?? this.memo,
        mobilePhone: mobilePhone ?? this.mobilePhone,
        name: name ?? this.name,
        orderCount: orderCount ?? this.orderCount,
        points: points ?? this.points,
        status: status ?? this.status,
        nicknameStore: nicknameStore ?? this.nicknameStore,
      );

  factory Member.fromRawJson(String str) => Member.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Member.fromJson(Map<String, dynamic> json) => Member(
        avatar: json["avatar"] == null ? null : json["avatar"],
        id: json["id"] == null ? null : json["id"],
        isVip: json["is_vip"] == null ? null : json["is_vip"],
        lastOrder: json["last_order"] == null
            ? null
            : LastOrder.fromJson(json["last_order"]),
        memberCouponCount: json["member_coupon_count"] == null
            ? null
            : List<MemberCouponCount>.from(json["member_coupon_count"]
                .map((x) => MemberCouponCount.fromJson(x))),
        memo: json["memo"] == null ? null : json["memo"],
        mobilePhone: json["mobile_phone"] == null ? null : json["mobile_phone"],
        name: json["name"] == null ? null : json["name"],
        orderCount: json["order_count"] == null ? null : json["order_count"],
        points: json["points"] == null ? null : json["points"],
        status: json["status"] == null ? null : json["status"],
        nicknameStore:
            json["nickname_store"] == null ? null : json["nickname_store"],
      );

  Map<String, dynamic> toJson() => {
        "avatar": avatar == null ? null : avatar,
        "id": id == null ? null : id,
        "is_vip": isVip == null ? null : isVip,
        "last_order": lastOrder == null ? null : lastOrder.toJson(),
        "member_coupon_count": memberCouponCount == null
            ? null
            : List<dynamic>.from(memberCouponCount.map((x) => x.toJson())),
        "memo": memo == null ? null : memo,
        "mobile_phone": mobilePhone == null ? null : mobilePhone,
        "name": name == null ? null : name,
        "order_count": orderCount == null ? null : orderCount,
        "points": points == null ? null : points,
        "status": status == null ? null : status,
        "nickname_store": nicknameStore == null ? null : nicknameStore,
      };
}

@Entity()
class LastOrder {
  LastOrder({
    this.createdAt,
    this.id,
    this.orderNumber,
  });

  String createdAt;
  @Id(assignable: true)
  int id;
  String orderNumber;

  LastOrder copyWith({
    String createdAt,
    num id,
    String orderNumber,
  }) =>
      LastOrder(
        createdAt: createdAt ?? this.createdAt,
        id: id ?? this.id,
        orderNumber: orderNumber ?? this.orderNumber,
      );

  factory LastOrder.fromRawJson(String str) =>
      LastOrder.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory LastOrder.fromJson(Map<String, dynamic> json) => LastOrder(
        createdAt: json["created_at"] == null ? null : json["created_at"],
        id: json["id"] == null ? null : json["id"],
        orderNumber: json["order_number"] == null ? null : json["order_number"],
      );

  Map<String, dynamic> toJson() => {
        "created_at": createdAt == null ? null : createdAt,
        "id": id == null ? null : id,
        "order_number": orderNumber == null ? null : orderNumber,
      };
}

class MemberCouponCount {
  MemberCouponCount({
    this.count,
    this.isOnline,
    this.kind,
  });

  num count;
  num isOnline;
  num kind;

  MemberCouponCount copyWith({
    num count,
    num isOnline,
    num kind,
  }) =>
      MemberCouponCount(
        count: count ?? this.count,
        isOnline: isOnline ?? this.isOnline,
        kind: kind ?? this.kind,
      );

  factory MemberCouponCount.fromRawJson(String str) =>
      MemberCouponCount.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MemberCouponCount.fromJson(Map<String, dynamic> json) =>
      MemberCouponCount(
        count: json["count"] == null ? null : json["count"],
        isOnline: json["is_online"] == null ? null : json["is_online"],
        kind: json["kind"] == null ? null : json["kind"],
      );

  Map<String, dynamic> toJson() => {
        "count": count == null ? null : count,
        "is_online": isOnline == null ? null : isOnline,
        "kind": kind == null ? null : kind,
      };
}
