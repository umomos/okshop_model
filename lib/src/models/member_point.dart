// To parse this JSON data, do
//
//     final memberPoint = memberPointFromJson(jsonString);

import 'dart:convert';

import 'package:objectbox/objectbox.dart';

import 'member.dart';
import 'order_summary.dart';

@Entity()
class MemberPoint {
  MemberPoint({
    this.comment,
    this.createdAt,
    this.expiryDate,
    this.id,
    this.memberId,
    this.orderId,
    this.points,
    this.storeAccount,
    this.type,
    this.updatedAt,
  });

  String comment;
  String createdAt;
  String expiryDate;
  @Id(assignable: true)
  int id;
  @Property(type: PropertyType.int)
  num memberId;
  @Property(type: PropertyType.int)
  num orderId;
  @Property(type: PropertyType.int)
  num points;
  String storeAccount;
  @Property(type: PropertyType.int)
  num type;
  String updatedAt;
  final customer = ToOne<Member>();
  final order = ToOne<OrderSummary>();

  MemberPoint copyWith({
    String comment,
    String createdAt,
    String expiryDate,
    num id,
    num memberId,
    num orderId,
    num points,
    String storeAccount,
    num type,
    String updatedAt,
  }) =>
      MemberPoint(
        comment: comment ?? this.comment,
        createdAt: createdAt ?? this.createdAt,
        expiryDate: expiryDate ?? this.expiryDate,
        id: id ?? this.id,
        memberId: memberId ?? this.memberId,
        orderId: orderId ?? this.orderId,
        points: points ?? this.points,
        storeAccount: storeAccount ?? this.storeAccount,
        type: type ?? this.type,
        updatedAt: updatedAt ?? this.updatedAt,
      );

  factory MemberPoint.fromRawJson(String str) =>
      MemberPoint.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MemberPoint.fromJson(Map<String, dynamic> json) => MemberPoint(
        comment: json["comment"],
        createdAt: json["created_at"],
        expiryDate: json["expiry_date"],
        id: json["id"],
        memberId: json["member_id"],
        orderId: json["order_id"],
        points: json["points"],
        storeAccount: json["store_account"],
        type: json["type"],
        updatedAt: json["updated_at"],
      );

  Map<String, dynamic> toJson() => {
        "comment": comment,
        "created_at": createdAt,
        "expiry_date": expiryDate,
        "id": id,
        "member_id": memberId,
        "order_id": orderId,
        "points": points,
        "store_account": storeAccount,
        "type": type,
        "updated_at": updatedAt,
      };
}
