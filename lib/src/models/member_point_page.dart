// To parse this JSON data, do
//
//     final revenuePage = revenuePageFromJson(jsonString);

import 'dart:convert';

import 'member_point.dart';
import 'pagination.dart';

class MemberPointPage {
  MemberPointPage({
    this.data,
    this.pagination,
    this.total,
  });

  List<MemberPoint> data;
  Pagination pagination;
  num total;

  MemberPointPage copyWith({
    List<MemberPoint> data,
    Pagination pagination,
  }) =>
      MemberPointPage(
        data: data ?? this.data,
        pagination: pagination ?? this.pagination,
        total: total ?? this.total,
      );

  factory MemberPointPage.fromRawJson(String str) =>
      MemberPointPage.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MemberPointPage.fromJson(Map<String, dynamic> json) =>
      MemberPointPage(
        data: json["data"] == null
            ? null
            : List<MemberPoint>.from(
                json["data"].map((x) => MemberPoint.fromJson(x))),
        pagination: json["pagination"] == null
            ? null
            : Pagination.fromJson(json["pagination"]),
        total: json["total"] == null ? null : json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "pagination": pagination == null ? null : pagination.toJson(),
        "total": total == null ? null : total,
      };
}
