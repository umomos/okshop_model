// To parse this JSON data, do
//
//     final memberPointReq = memberPointReqFromJson(jsonString);

import 'dart:convert';

class MemberPointQry {
  MemberPointQry({
    this.memberId,
    this.orderId,
    this.type,
    this.comment,
  });

  num memberId;
  num orderId;
  num type;
  String comment;

  MemberPointQry copyWith({
    num memberId,
    num orderId,
    num type,
    String comment,
  }) =>
      MemberPointQry(
        memberId: memberId ?? this.memberId,
        orderId: orderId ?? this.orderId,
        type: type ?? this.type,
        comment: comment ?? this.comment,
      );

  factory MemberPointQry.fromRawJson(String str) =>
      MemberPointQry.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MemberPointQry.fromJson(Map<String, dynamic> json) => MemberPointQry(
        memberId: json["member_id"],
        orderId: json["order_id"],
        type: json["type"],
        comment: json["comment"],
      );

  Map<String, dynamic> toJson() => {
        "member_id": memberId,
        "order_id": orderId,
        "type": type,
        "comment": comment,
      };
}
