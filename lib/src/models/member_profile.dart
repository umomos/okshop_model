// To parse this JSON data, do
//
//     final memberProfile = memberProfileFromJson(jsonString);

import 'dart:convert';

import 'address.dart';

class MemberProfile {
  MemberProfile({
    this.addresses,
    this.avatar,
    this.birthday,
    this.email,
    this.gender,
    this.id,
    this.invoiceCarrier,
    this.mobilePhone,
    this.name,
    this.nicknameStore,
    this.status,
    this.updatedAt,
    this.npoBan,
    this.vatNumber,
  });

  Address addresses;
  String avatar;
  String birthday;
  String email;
  num gender;
  num id;
  String invoiceCarrier;
  String mobilePhone;
  String name;
  String nicknameStore;
  num status;
  String updatedAt;
  String npoBan;
  String vatNumber;

  MemberProfile copyWith({
    Address addresses,
    String avatar,
    String birthday,
    String email,
    num gender,
    num id,
    String invoiceCarrier,
    String mobilePhone,
    String name,
    String nicknameStore,
    num status,
    String updatedAt,
    String npoBan,
    String vatNumber,
  }) =>
      MemberProfile(
        addresses: addresses ?? this.addresses,
        avatar: avatar ?? this.avatar,
        birthday: birthday ?? this.birthday,
        email: email ?? this.email,
        gender: gender ?? this.gender,
        id: id ?? this.id,
        invoiceCarrier: invoiceCarrier ?? this.invoiceCarrier,
        mobilePhone: mobilePhone ?? this.mobilePhone,
        name: name ?? this.name,
        nicknameStore: nicknameStore ?? this.nicknameStore,
        status: status ?? this.status,
        updatedAt: updatedAt ?? this.updatedAt,
        npoBan: npoBan ?? this.npoBan,
        vatNumber: vatNumber ?? this.vatNumber,
      );

  factory MemberProfile.fromRawJson(String str) =>
      MemberProfile.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory MemberProfile.fromJson(Map<String, dynamic> json) => MemberProfile(
        addresses: json["addresses"] == null
            ? null
            : Address.fromJson(json["addresses"]),
        avatar: json["avatar"] == null ? null : json["avatar"],
        birthday: json["birthday"] == null ? null : json["birthday"],
        email: json["email"] == null ? null : json["email"],
        gender: json["gender"] == null ? null : json["gender"],
        id: json["id"] == null ? null : json["id"],
        invoiceCarrier:
            json["invoice_carrier"] == null ? null : json["invoice_carrier"],
        mobilePhone: json["mobile_phone"] == null ? null : json["mobile_phone"],
        name: json["name"] == null ? null : json["name"],
        nicknameStore:
            json["nickname_store"] == null ? null : json["nickname_store"],
        status: json["status"] == null ? null : json["status"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
        npoBan: json["npo_ban"] == null ? null : json["npo_ban"],
        vatNumber: json["vat_number"] == null ? null : json["vat_number"],
      );

  Map<String, dynamic> toJson() => {
        "addresses": addresses == null ? null : addresses.toJson(),
        "avatar": avatar == null ? null : avatar,
        "birthday": birthday == null ? null : birthday,
        "email": email == null ? null : email,
        "gender": gender == null ? null : gender,
        "id": id == null ? null : id,
        "invoice_carrier": invoiceCarrier == null ? null : invoiceCarrier,
        "mobile_phone": mobilePhone == null ? null : mobilePhone,
        "name": name == null ? null : name,
        "nickname_store": nicknameStore == null ? null : nicknameStore,
        "status": status == null ? null : status,
        "updated_at": updatedAt == null ? null : updatedAt,
        "npo_ban": npoBan == null ? null : npoBan,
        "vat_number": vatNumber == null ? null : vatNumber,
      };
}
