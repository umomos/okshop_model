// To parse this JSON data, do
//
//     final revenuePage = revenuePageFromJson(jsonString);

import 'dart:convert';

import 'order_summary.dart';
import 'pagination.dart';

class OrderPage {
  OrderPage({
    this.data,
    this.pagination,
  });

  List<OrderSummary> data;
  Pagination pagination;

  OrderPage copyWith({
    List<OrderSummary> data,
    Pagination pagination,
  }) =>
      OrderPage(
        data: data ?? this.data,
        pagination: pagination ?? this.pagination,
      );

  factory OrderPage.fromRawJson(String str) =>
      OrderPage.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory OrderPage.fromJson(Map<String, dynamic> json) => OrderPage(
        data: json["data"] == null
            ? null
            : List<OrderSummary>.from(
                json["data"].map((x) => OrderSummary.fromJson(x))),
        pagination: json["pagination"] == null
            ? null
            : Pagination.fromJson(json["pagination"]),
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "pagination": pagination == null ? null : pagination.toJson(),
      };
}
