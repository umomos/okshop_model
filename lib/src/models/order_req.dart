// To parse this JSON data, do
//
//     final orderReq = orderReqFromJson(jsonString);

import 'dart:convert';

class OrderReq {
  OrderReq({
    this.page,
    this.limit,
    this.memberId,
    this.table1Id,
    this.table2Id,
    this.source,
    this.type,
    this.startTime,
    this.endTime,
    this.status,
    this.keyword,
    this.sortType,
    this.sort,
    this.isPrint,
    this.mobilePhone,
    this.name,
    this.orderNumber,
    this.withSubOrder,
  });

  num page;
  num limit;
  num memberId;
  num table1Id;
  num table2Id;
  num source;
  List<num> type;
  String startTime;
  String endTime;
  List<num> status;
  String keyword;
  String sortType;
  String sort;
  num isPrint;
  String mobilePhone;
  String name;
  String orderNumber;
  num withSubOrder;

  OrderReq copyWith({
    num page,
    num limit,
    num memberId,
    num table1Id,
    num table2Id,
    num source,
    List<num> type,
    String startTime,
    String endTime,
    List<num> status,
    String keyword,
    String sortType,
    String sort,
    num isPrint,
    String mobilePhone,
    String name,
    String orderNumber,
    num withSubOrder,
  }) =>
      OrderReq(
        page: page ?? this.page,
        limit: limit ?? this.limit,
        memberId: memberId ?? this.memberId,
        table1Id: table1Id ?? this.table1Id,
        table2Id: table2Id ?? this.table2Id,
        source: source ?? this.source,
        type: type ?? this.type,
        startTime: startTime ?? this.startTime,
        endTime: endTime ?? this.endTime,
        status: status ?? this.status,
        keyword: keyword ?? this.keyword,
        sortType: sortType ?? this.sortType,
        sort: sort ?? this.sort,
        isPrint: isPrint ?? this.isPrint,
        mobilePhone: mobilePhone ?? this.mobilePhone,
        name: name ?? this.name,
        orderNumber: orderNumber ?? this.orderNumber,
        withSubOrder: withSubOrder ?? this.withSubOrder,
      );

  factory OrderReq.fromRawJson(String str) =>
      OrderReq.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory OrderReq.fromJson(Map<String, dynamic> json) => OrderReq(
        page: json["page"],
        limit: json["limit"],
        memberId: json["member_id"],
        table1Id: json["table1_id"],
        table2Id: json["table2_id"],
        source: json["source"],
        type: json["type"] == null
            ? null
            : List<num>.from(jsonDecode(json["type"]).map((x) => x)),
        startTime: json["start_time"],
        endTime: json["end_time"],
        status: json["status"] == null
            ? null
            : List<num>.from(jsonDecode(json["status"]).map((x) => x)),
        keyword: json["keyword"],
        sortType: json["sort_type"],
        sort: json["sort"],
        isPrint: json["is_print"],
        mobilePhone: json["mobile_phone"],
        name: json["name"],
        orderNumber: json["order_number"],
        withSubOrder: json["with_sub_order"],
      );

  Map<String, dynamic> toJson() => {
        "page": page,
        "limit": limit,
        "member_id": memberId,
        "table1_id": table1Id,
        "table2_id": table2Id,
        "source": source,
        "type": type == null ? null : jsonEncode(type),
        "start_time": startTime,
        "end_time": endTime,
        "status": status == null ? null : jsonEncode(status),
        "keyword": keyword,
        "sort_type": sortType,
        "sort": sort,
        "is_print": isPrint,
        "mobile_phone": mobilePhone,
        "name": name,
        "order_number": orderNumber,
        "with_sub_order": withSubOrder,
      };
}
