// To parse this JSON data, do
//
//     final orderSelectorFilter = orderSelectorFilterFromJson(jsonString);

import 'dart:convert';

// TODO: try remove me, use order req instead.
class OrderSelectorFilter {
  OrderSelectorFilter({
    this.table1Id,
    this.table2Id,
    this.id,
    this.table1Name,
    this.table2Name,
    this.last3Digits,
    this.sourceOrKind,
  });

  num table1Id;
  num table2Id;
  num id;
  String table1Name;
  String table2Name;
  String last3Digits;
  num sourceOrKind;

  OrderSelectorFilter copyWith({
    num table1Id,
    num table2Id,
    num id,
    String table1Name,
    String table2Name,
    String last3Digits,
    num sourceOrKind,
  }) =>
      OrderSelectorFilter(
        table1Id: table1Id ?? this.table1Id,
        table2Id: table2Id ?? this.table2Id,
        id: id ?? this.id,
        table1Name: table1Name ?? this.table1Name,
        table2Name: table2Name ?? this.table2Name,
        last3Digits: last3Digits ?? this.last3Digits,
        sourceOrKind: sourceOrKind ?? this.sourceOrKind,
      );

  factory OrderSelectorFilter.fromRawJson(String str) =>
      OrderSelectorFilter.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory OrderSelectorFilter.fromJson(Map<String, dynamic> json) =>
      OrderSelectorFilter(
        table1Id: json["table1_id"],
        table2Id: json["table2_id"],
        id: json["id"],
        table1Name: json["table_1_name"],
        table2Name: json["table_2_name"],
        last3Digits: json["last_3_digits"],
        sourceOrKind: json["source_or_kind"],
      );

  Map<String, dynamic> toJson() => {
        "table1_id": table1Id,
        "table2_id": table2Id,
        "id": id,
        "table_1_name": table1Name,
        "table_2_name": table2Name,
        "last_3_digits": last3Digits,
        "source_or_kind": sourceOrKind,
      };
}
