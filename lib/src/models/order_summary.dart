// To parse this JSON data, do
//
//     final orderSummary = orderSummaryFromJson(jsonString);

import 'dart:convert';

import 'package:objectbox/objectbox.dart';

import 'member.dart';

@Entity()
class OrderSummary {
  OrderSummary({
    this.adult,
    this.child,
    this.createdAt,
    this.id,
    this.isPrint,
    this.itemCount,
    this.masterId,
    this.mealAt,
    this.member,
    this.orderNumber,
    this.paymentStatus,
    this.source,
    this.status,
    this.table1Id,
    this.table1Name,
    this.table2Id,
    this.table2Name,
    this.total,
    this.type,
    this.updatedAt,
    this.usingCoupon,
    this.usingPoints,
  });

  @Property(type: PropertyType.int)
  num adult;
  @Property(type: PropertyType.int)
  num child;
  String createdAt;
  @Id(assignable: true)
  int id;
  @Property(type: PropertyType.int)
  num isPrint;
  @Property(type: PropertyType.int)
  num itemCount;
  @Property(type: PropertyType.int)
  num masterId;
  String mealAt;
  Member member;
  String orderNumber;
  @Property(type: PropertyType.int)
  num paymentStatus;
  @Property(type: PropertyType.int)
  num source;
  @Property(type: PropertyType.int)
  num status;
  @Property(type: PropertyType.int)
  num table1Id;
  String table1Name;
  @Property(type: PropertyType.int)
  num table2Id;
  String table2Name;
  @Property(type: PropertyType.float)
  num total;
  @Property(type: PropertyType.int)
  num type;
  String updatedAt;
  bool usingCoupon;
  bool usingPoints;
  String rawJson;
  @Property(type: PropertyType.int)
  num timestamp;
  final customer = ToOne<Member>();

  OrderSummary copyWith({
    num adult,
    num child,
    String createdAt,
    num id,
    num isPrint,
    num itemCount,
    num masterId,
    String mealAt,
    Member member,
    String orderNumber,
    num paymentStatus,
    num source,
    num status,
    num table1Id,
    String table1Name,
    num table2Id,
    String table2Name,
    num total,
    num type,
    String updatedAt,
    bool usingCoupon,
    bool usingPoints,
  }) =>
      OrderSummary(
        adult: adult ?? this.adult,
        child: child ?? this.child,
        createdAt: createdAt ?? this.createdAt,
        id: id ?? this.id,
        isPrint: isPrint ?? this.isPrint,
        itemCount: itemCount ?? this.itemCount,
        masterId: masterId ?? this.masterId,
        mealAt: mealAt ?? this.mealAt,
        member: member ?? this.member,
        orderNumber: orderNumber ?? this.orderNumber,
        paymentStatus: paymentStatus ?? this.paymentStatus,
        source: source ?? this.source,
        status: status ?? this.status,
        table1Id: table1Id ?? this.table1Id,
        table1Name: table1Name ?? this.table1Name,
        table2Id: table2Id ?? this.table2Id,
        table2Name: table2Name ?? this.table2Name,
        total: total ?? this.total,
        type: type ?? this.type,
        updatedAt: updatedAt ?? this.updatedAt,
        usingCoupon: usingCoupon ?? this.usingCoupon,
        usingPoints: usingPoints ?? this.usingPoints,
      );

  factory OrderSummary.fromRawJson(String str) =>
      OrderSummary.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory OrderSummary.fromJson(Map<String, dynamic> json) => OrderSummary(
        adult: json["adult"],
        child: json["child"],
        createdAt: json["created_at"],
        id: json["id"],
        isPrint: json["is_print"],
        itemCount: json["item_count"],
        masterId: json["master_id"],
        mealAt: json["meal_at"],
        member: json["member"] == null ? null : Member.fromJson(json["member"]),
        orderNumber: json["order_number"],
        paymentStatus: json["payment_status"],
        source: json["source"],
        status: json["status"],
        table1Id: json["table1_id"],
        table1Name: json["table1_name"],
        table2Id: json["table2_id"],
        table2Name: json["table2_name"],
        total: json["total"],
        type: json["type"],
        updatedAt: json["updated_at"],
        usingCoupon: json["using_coupon"],
        usingPoints: json["using_points"],
      );

  Map<String, dynamic> toJson() => {
        "adult": adult,
        "child": child,
        "created_at": createdAt,
        "id": id,
        "is_print": isPrint,
        "item_count": itemCount,
        "master_id": masterId,
        "meal_at": mealAt,
        "member": member?.toJson(),
        "order_number": orderNumber,
        "payment_status": paymentStatus,
        "source": source,
        "status": status,
        "table1_id": table1Id,
        "table1_name": table1Name,
        "table2_id": table2Id,
        "table2_name": table2Name,
        "total": total,
        "type": type,
        "updated_at": updatedAt,
        "using_coupon": usingCoupon,
        "using_points": usingPoints,
      };
}

// class Member {
//   Member({
//     this.avatar,
//     this.id,
//     this.name,
//     this.nicknameStore,
//   });

//   String avatar;
//   num id;
//   String name;
//   String nicknameStore;

//   Member copyWith({
//     String avatar,
//     num id,
//     String name,
//     String nicknameStore,
//   }) =>
//       Member(
//         avatar: avatar ?? this.avatar,
//         id: id ?? this.id,
//         name: name ?? this.name,
//         nicknameStore: nicknameStore ?? this.nicknameStore,
//       );

//   factory Member.fromRawJson(String str) => Member.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory Member.fromJson(Map<String, dynamic> json) => Member(
//         avatar: json["avatar"],
//         id: json["id"],
//         name: json["name"],
//         nicknameStore: json["nickname_store"],
//       );

//   Map<String, dynamic> toJson() => {
//         "avatar": avatar,
//         "id": id,
//         "name": name,
//         "nickname_store": nicknameStore,
//       };
// }
