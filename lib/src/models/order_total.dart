// To parse this JSON data, do
//
//     final orderTotal = orderTotalFromJson(jsonString);

import 'dart:convert';

class OrderTotal {
  OrderTotal({
    this.diner,
    this.dinerOnline,
    this.shop,
    this.shopOnline,
    this.total,
  });

  Diner diner;
  Diner dinerOnline;
  Shop shop;
  Shop shopOnline;
  Total total;

  OrderTotal copyWith({
    Diner diner,
    Diner dinerOnline,
    Shop shop,
    Shop shopOnline,
    Total total,
  }) =>
      OrderTotal(
        diner: diner ?? this.diner,
        dinerOnline: dinerOnline ?? this.dinerOnline,
        shop: shop ?? this.shop,
        shopOnline: shopOnline ?? this.shopOnline,
        total: total ?? this.total,
      );

  factory OrderTotal.fromRawJson(String str) =>
      OrderTotal.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory OrderTotal.fromJson(Map<String, dynamic> json) => OrderTotal(
        diner: json["diner"] == null ? null : Diner.fromJson(json["diner"]),
        dinerOnline: json["diner_online"] == null
            ? null
            : Diner.fromJson(json["diner_online"]),
        shop: json["shop"] == null ? null : Shop.fromJson(json["shop"]),
        shopOnline: json["shop_online"] == null
            ? null
            : Shop.fromJson(json["shop_online"]),
        total: json["total"] == null ? null : Total.fromJson(json["total"]),
      );

  Map<String, dynamic> toJson() => {
        "diner": diner == null ? null : diner.toJson(),
        "diner_online": dinerOnline == null ? null : dinerOnline.toJson(),
        "shop": shop == null ? null : shop.toJson(),
        "shop_online": shopOnline == null ? null : shopOnline.toJson(),
        "total": total == null ? null : total.toJson(),
      };
}

class Diner {
  Diner({
    this.next,
    this.now,
  });

  DinerNext next;
  DinerNext now;

  Diner copyWith({
    DinerNext next,
    DinerNext now,
  }) =>
      Diner(
        next: next ?? this.next,
        now: now ?? this.now,
      );

  factory Diner.fromRawJson(String str) => Diner.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Diner.fromJson(Map<String, dynamic> json) => Diner(
        next: json["next"] == null ? null : DinerNext.fromJson(json["next"]),
        now: json["now"] == null ? null : DinerNext.fromJson(json["now"]),
      );

  Map<String, dynamic> toJson() => {
        "next": next == null ? null : next.toJson(),
        "now": now == null ? null : now.toJson(),
      };
}

class DinerNext {
  DinerNext({
    this.delivery,
    this.dinerIn,
    this.toGo,
  });

  num delivery;
  num dinerIn;
  num toGo;

  DinerNext copyWith({
    num delivery,
    num dinerIn,
    num toGo,
  }) =>
      DinerNext(
        delivery: delivery ?? this.delivery,
        dinerIn: dinerIn ?? this.dinerIn,
        toGo: toGo ?? this.toGo,
      );

  factory DinerNext.fromRawJson(String str) =>
      DinerNext.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory DinerNext.fromJson(Map<String, dynamic> json) => DinerNext(
        delivery: json["delivery"] == null ? null : json["delivery"],
        dinerIn: json["diner_in"] == null ? null : json["diner_in"],
        toGo: json["to_go"] == null ? null : json["to_go"],
      );

  Map<String, dynamic> toJson() => {
        "delivery": delivery == null ? null : delivery,
        "diner_in": dinerIn == null ? null : dinerIn,
        "to_go": toGo == null ? null : toGo,
      };
}

class Shop {
  Shop({
    this.next,
    this.now,
  });

  ShopNext next;
  ShopNext now;

  Shop copyWith({
    ShopNext next,
    ShopNext now,
  }) =>
      Shop(
        next: next ?? this.next,
        now: now ?? this.now,
      );

  factory Shop.fromRawJson(String str) => Shop.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Shop.fromJson(Map<String, dynamic> json) => Shop(
        next: json["next"] == null ? null : ShopNext.fromJson(json["next"]),
        now: json["now"] == null ? null : ShopNext.fromJson(json["now"]),
      );

  Map<String, dynamic> toJson() => {
        "next": next == null ? null : next.toJson(),
        "now": now == null ? null : now.toJson(),
      };
}

class ShopNext {
  ShopNext({
    this.delivery,
    this.instore,
  });

  num delivery;
  num instore;

  ShopNext copyWith({
    num delivery,
    num instore,
  }) =>
      ShopNext(
        delivery: delivery ?? this.delivery,
        instore: instore ?? this.instore,
      );

  factory ShopNext.fromRawJson(String str) =>
      ShopNext.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ShopNext.fromJson(Map<String, dynamic> json) => ShopNext(
        delivery: json["delivery"] == null ? null : json["delivery"],
        instore: json["instore"] == null ? null : json["instore"],
      );

  Map<String, dynamic> toJson() => {
        "delivery": delivery == null ? null : delivery,
        "instore": instore == null ? null : instore,
      };
}

class Total {
  Total({
    this.dinerNext,
    this.dinerNow,
    this.dinerOnlineNext,
    this.dinerOnlineNow,
    this.shopNext,
    this.shopNow,
    this.shopOnlineNext,
    this.shopOnlineNow,
  });

  num dinerNext;
  num dinerNow;
  num dinerOnlineNext;
  num dinerOnlineNow;
  num shopNext;
  num shopNow;
  num shopOnlineNext;
  num shopOnlineNow;

  Total copyWith({
    num dinerNext,
    num dinerNow,
    num dinerOnlineNext,
    num dinerOnlineNow,
    num shopNext,
    num shopNow,
    num shopOnlineNext,
    num shopOnlineNow,
  }) =>
      Total(
        dinerNext: dinerNext ?? this.dinerNext,
        dinerNow: dinerNow ?? this.dinerNow,
        dinerOnlineNext: dinerOnlineNext ?? this.dinerOnlineNext,
        dinerOnlineNow: dinerOnlineNow ?? this.dinerOnlineNow,
        shopNext: shopNext ?? this.shopNext,
        shopNow: shopNow ?? this.shopNow,
        shopOnlineNext: shopOnlineNext ?? this.shopOnlineNext,
        shopOnlineNow: shopOnlineNow ?? this.shopOnlineNow,
      );

  factory Total.fromRawJson(String str) => Total.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Total.fromJson(Map<String, dynamic> json) => Total(
        dinerNext: json["diner_next"] == null ? null : json["diner_next"],
        dinerNow: json["diner_now"] == null ? null : json["diner_now"],
        dinerOnlineNext: json["diner_online_next"] == null
            ? null
            : json["diner_online_next"],
        dinerOnlineNow:
            json["diner_online_now"] == null ? null : json["diner_online_now"],
        shopNext: json["shop_next"] == null ? null : json["shop_next"],
        shopNow: json["shop_now"] == null ? null : json["shop_now"],
        shopOnlineNext:
            json["shop_online_next"] == null ? null : json["shop_online_next"],
        shopOnlineNow:
            json["shop_online_now"] == null ? null : json["shop_online_now"],
      );

  Map<String, dynamic> toJson() => {
        "diner_next": dinerNext == null ? null : dinerNext,
        "diner_now": dinerNow == null ? null : dinerNow,
        "diner_online_next": dinerOnlineNext == null ? null : dinerOnlineNext,
        "diner_online_now": dinerOnlineNow == null ? null : dinerOnlineNow,
        "shop_next": shopNext == null ? null : shopNext,
        "shop_now": shopNow == null ? null : shopNow,
        "shop_online_next": shopOnlineNext == null ? null : shopOnlineNext,
        "shop_online_now": shopOnlineNow == null ? null : shopOnlineNow,
      };
}
