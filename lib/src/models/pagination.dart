// To parse this JSON data, do
//
//     final pagination = paginationFromJson(jsonString);

import 'dart:convert';

class Pagination {
  Pagination({
    this.currentPage,
    this.from,
    this.lastPage,
    this.prePage,
    this.to,
    this.total,
  });

  num currentPage;
  num from;
  num lastPage;
  num prePage;
  num to;
  num total;

  Pagination copyWith({
    num currentPage,
    num from,
    num lastPage,
    num prePage,
    num to,
    num total,
  }) =>
      Pagination(
        currentPage: currentPage ?? this.currentPage,
        from: from ?? this.from,
        lastPage: lastPage ?? this.lastPage,
        prePage: prePage ?? this.prePage,
        to: to ?? this.to,
        total: total ?? this.total,
      );

  factory Pagination.fromRawJson(String str) =>
      Pagination.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
        currentPage: json["current_page"] == null ? null : json["current_page"],
        from: json["from"] == null ? null : json["from"],
        lastPage: json["last_page"] == null ? null : json["last_page"],
        prePage: json["pre_page"] == null ? null : json["pre_page"],
        to: json["to"] == null ? null : json["to"],
        total: json["total"] == null ? null : json["total"],
      );

  Map<String, dynamic> toJson() => {
        "current_page": currentPage == null ? null : currentPage,
        "from": from == null ? null : from,
        "last_page": lastPage == null ? null : lastPage,
        "pre_page": prePage == null ? null : prePage,
        "to": to == null ? null : to,
        "total": total == null ? null : total,
      };
}
