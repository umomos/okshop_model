// To parse this JSON data, do
//
//     final passwordReset = passwordResetFromJson(jsonString);

import 'dart:convert';

class PasswordReset {
  PasswordReset({
    this.oldPassword,
    this.newPassword,
    this.checkPassword,
  });

  String oldPassword;
  String newPassword;
  String checkPassword;

  PasswordReset copyWith({
    String oldPassword,
    String newPassword,
    String checkPassword,
  }) =>
      PasswordReset(
        oldPassword: oldPassword ?? this.oldPassword,
        newPassword: newPassword ?? this.newPassword,
        checkPassword: checkPassword ?? this.checkPassword,
      );

  factory PasswordReset.fromRawJson(String str) =>
      PasswordReset.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PasswordReset.fromJson(Map<String, dynamic> json) => PasswordReset(
        oldPassword: json["old_password"] == null ? null : json["old_password"],
        newPassword: json["new_password"] == null ? null : json["new_password"],
        checkPassword:
            json["check_password"] == null ? null : json["check_password"],
      );

  Map<String, dynamic> toJson() => {
        "old_password": oldPassword == null ? null : oldPassword,
        "new_password": newPassword == null ? null : newPassword,
        "check_password": checkPassword == null ? null : checkPassword,
      };
}
