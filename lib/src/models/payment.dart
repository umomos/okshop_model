// To parse this JSON data, do
//
//     final payment = paymentFromJson(jsonString);

import 'dart:convert';

class Payment {
  Payment({
    this.channelPayMethodName,
    this.expenses,
    this.income,
  });

  String channelPayMethodName;
  num expenses;
  num income;

  Payment copyWith({
    String channelPayMethodName,
    num expenses,
    num income,
  }) =>
      Payment(
        channelPayMethodName: channelPayMethodName ?? this.channelPayMethodName,
        expenses: expenses ?? this.expenses,
        income: income ?? this.income,
      );

  factory Payment.fromRawJson(String str) => Payment.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        channelPayMethodName: json["channel_pay_method_name"] == null
            ? null
            : json["channel_pay_method_name"],
        expenses: json["expenses"] == null ? null : json["expenses"],
        income: json["income"] == null ? null : json["income"],
      );

  Map<String, dynamic> toJson() => {
        "channel_pay_method_name":
            channelPayMethodName == null ? null : channelPayMethodName,
        "expenses": expenses == null ? null : expenses,
        "income": income == null ? null : income,
      };
}
