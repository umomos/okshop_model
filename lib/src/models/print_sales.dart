// To parse this JSON data, do
//
//     final printSales = printSalesFromJson(jsonString);

import 'dart:convert';

class PrintSales {
  PrintSales({
    this.title,
    this.subject,
    this.date,
    this.storeName,
    this.user,
    this.app,
    this.online,
    this.appTotal,
    this.appQuantity,
    this.onlineTotal,
    this.onlineQuantity,
  });

  String title;
  String subject;
  String date;
  String storeName;
  String user;
  List<App> app;
  List<App> online;
  num appTotal;
  num appQuantity;
  num onlineTotal;
  num onlineQuantity;

  PrintSales copyWith({
    String title,
    String subject,
    String date,
    String storeName,
    String user,
    List<App> app,
    List<App> online,
    num appTotal,
    num appQuantity,
    num onlineTotal,
    num onlineQuantity,
  }) =>
      PrintSales(
        title: title ?? this.title,
        subject: subject ?? this.subject,
        date: date ?? this.date,
        storeName: storeName ?? this.storeName,
        user: user ?? this.user,
        app: app ?? this.app,
        online: online ?? this.online,
        appTotal: appTotal ?? this.appTotal,
        appQuantity: appQuantity ?? this.appQuantity,
        onlineTotal: onlineTotal ?? this.onlineTotal,
        onlineQuantity: onlineQuantity ?? this.onlineQuantity,
      );

  factory PrintSales.fromRawJson(String str) =>
      PrintSales.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PrintSales.fromJson(Map<String, dynamic> json) => PrintSales(
        title: json["title"] == null ? null : json["title"],
        subject: json["subject"] == null ? null : json["subject"],
        date: json["date"] == null ? null : json["date"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        user: json["user"] == null ? null : json["user"],
        app: json["app"] == null
            ? null
            : List<App>.from(json["app"].map((x) => App.fromJson(x))),
        online: json["online"] == null
            ? null
            : List<App>.from(json["online"].map((x) => App.fromJson(x))),
        appTotal: json["app_total"] == null ? null : json["app_total"],
        appQuantity: json["app_quantity"] == null ? null : json["app_quantity"],
        onlineTotal: json["online_total"] == null ? null : json["online_total"],
        onlineQuantity:
            json["online_quantity"] == null ? null : json["online_quantity"],
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "subject": subject == null ? null : subject,
        "date": date == null ? null : date,
        "store_name": storeName == null ? null : storeName,
        "user": user == null ? null : user,
        "app":
            app == null ? null : List<dynamic>.from(app.map((x) => x.toJson())),
        "online": online == null
            ? null
            : List<dynamic>.from(online.map((x) => x.toJson())),
        "app_total": appTotal == null ? null : appTotal,
        "app_quantity": appQuantity == null ? null : appQuantity,
        "online_total": onlineTotal == null ? null : onlineTotal,
        "online_quantity": onlineQuantity == null ? null : onlineQuantity,
      };
}

class App {
  App({
    this.price,
    this.productTitle,
    this.quantity,
    this.total,
    this.updatedAt,
  });

  num price;
  String productTitle;
  num quantity;
  num total;
  String updatedAt;

  App copyWith({
    num price,
    String productTitle,
    num quantity,
    num total,
    String updatedAt,
  }) =>
      App(
        price: price ?? this.price,
        productTitle: productTitle ?? this.productTitle,
        quantity: quantity ?? this.quantity,
        total: total ?? this.total,
        updatedAt: updatedAt ?? this.updatedAt,
      );

  factory App.fromRawJson(String str) => App.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory App.fromJson(Map<String, dynamic> json) => App(
        price: json["price"] == null ? null : json["price"],
        productTitle:
            json["product_title"] == null ? null : json["product_title"],
        quantity: json["quantity"] == null ? null : json["quantity"],
        total: json["total"] == null ? null : json["total"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
      );

  Map<String, dynamic> toJson() => {
        "price": price == null ? null : price,
        "product_title": productTitle == null ? null : productTitle,
        "quantity": quantity == null ? null : quantity,
        "total": total == null ? null : total,
        "updated_at": updatedAt == null ? null : updatedAt,
      };
}
