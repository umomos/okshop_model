// To parse this JSON data, do
//
//     final printStatements = printStatementsFromJson(jsonString);

import 'dart:convert';

import 'payment.dart';
import 'refund_order.dart';

class PrintStatements {
  PrintStatements({
    this.title,
    this.subject,
    this.date,
    this.storeName,
    this.user,
    this.orderAmount,
    this.orderTax,
    this.orderTotal,
    this.payment,
    this.orderNumberStart,
    this.orderNumberEnd,
    this.appOrderQuantity,
    this.appOrderTotal,
    this.lineOrderQuantity,
    this.lineOrderTotal,
    this.onSiteDiscount,
    this.extraCharges,
    this.refundOrders,
    this.refundQuantity,
    this.refundAmount,
    this.invoiceNumberStart,
    this.invoiceNumberEnd,
    this.invoiceQuantity,
    this.invoiceAmount,
    this.invoiceVoidQuantity,
    this.invoiceVoidAmount,
    this.invoiceVoidNumber,
  });

  String title;
  String subject;
  String date;
  String storeName;
  String user;
  num orderAmount;
  num orderTax;
  num orderTotal;
  List<Payment> payment;
  String orderNumberStart;
  String orderNumberEnd;
  num appOrderQuantity;
  num appOrderTotal;
  num lineOrderQuantity;
  num lineOrderTotal;
  num onSiteDiscount;
  num extraCharges;
  List<RefundOrder> refundOrders;
  num refundQuantity;
  num refundAmount;
  String invoiceNumberStart;
  String invoiceNumberEnd;
  num invoiceQuantity;
  num invoiceAmount;
  num invoiceVoidQuantity;
  num invoiceVoidAmount;
  List<String> invoiceVoidNumber;

  PrintStatements copyWith({
    String title,
    String subject,
    String date,
    String storeName,
    String user,
    num orderAmount,
    num orderTax,
    num orderTotal,
    List<Payment> payment,
    String orderNumberStart,
    String orderNumberEnd,
    num appOrderQuantity,
    num appOrderTotal,
    num lineOrderQuantity,
    num lineOrderTotal,
    num onSiteDiscount,
    num extraCharges,
    List<RefundOrder> refundOrders,
    num refundQuantity,
    num refundAmount,
    String invoiceNumberStart,
    String invoiceNumberEnd,
    num invoiceQuantity,
    num invoiceAmount,
    num invoiceVoidQuantity,
    num invoiceVoidAmount,
    List<String> invoiceVoidNumber,
  }) =>
      PrintStatements(
        title: title ?? this.title,
        subject: subject ?? this.subject,
        date: date ?? this.date,
        storeName: storeName ?? this.storeName,
        user: user ?? this.user,
        orderAmount: orderAmount ?? this.orderAmount,
        orderTax: orderTax ?? this.orderTax,
        orderTotal: orderTotal ?? this.orderTotal,
        payment: payment ?? this.payment,
        orderNumberStart: orderNumberStart ?? this.orderNumberStart,
        orderNumberEnd: orderNumberEnd ?? this.orderNumberEnd,
        appOrderQuantity: appOrderQuantity ?? this.appOrderQuantity,
        appOrderTotal: appOrderTotal ?? this.appOrderTotal,
        lineOrderQuantity: lineOrderQuantity ?? this.lineOrderQuantity,
        lineOrderTotal: lineOrderTotal ?? this.lineOrderTotal,
        onSiteDiscount: onSiteDiscount ?? this.onSiteDiscount,
        extraCharges: extraCharges ?? this.extraCharges,
        refundOrders: refundOrders ?? this.refundOrders,
        refundQuantity: refundQuantity ?? this.refundQuantity,
        refundAmount: refundAmount ?? this.refundAmount,
        invoiceNumberStart: invoiceNumberStart ?? this.invoiceNumberStart,
        invoiceNumberEnd: invoiceNumberEnd ?? this.invoiceNumberEnd,
        invoiceQuantity: invoiceQuantity ?? this.invoiceQuantity,
        invoiceAmount: invoiceAmount ?? this.invoiceAmount,
        invoiceVoidQuantity: invoiceVoidQuantity ?? this.invoiceVoidQuantity,
        invoiceVoidAmount: invoiceVoidAmount ?? this.invoiceVoidAmount,
        invoiceVoidNumber: invoiceVoidNumber ?? this.invoiceVoidNumber,
      );

  factory PrintStatements.fromRawJson(String str) =>
      PrintStatements.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory PrintStatements.fromJson(Map<String, dynamic> json) =>
      PrintStatements(
        title: json["title"] == null ? null : json["title"],
        subject: json["subject"] == null ? null : json["subject"],
        date: json["date"] == null ? null : json["date"],
        storeName: json["store_name"] == null ? null : json["store_name"],
        user: json["user"] == null ? null : json["user"],
        orderAmount: json["order_amount"] == null ? null : json["order_amount"],
        orderTax: json["order_tax"] == null ? null : json["order_tax"],
        orderTotal: json["order_total"] == null ? null : json["order_total"],
        payment: json["payment"] == null
            ? null
            : List<Payment>.from(
                json["payment"].map((x) => Payment.fromJson(x))),
        orderNumberStart: json["order_number_start"] == null
            ? null
            : json["order_number_start"],
        orderNumberEnd:
            json["order_number_end"] == null ? null : json["order_number_end"],
        appOrderQuantity: json["app_order_quantity"] == null
            ? null
            : json["app_order_quantity"],
        appOrderTotal:
            json["app_order_total"] == null ? null : json["app_order_total"],
        lineOrderQuantity: json["line_order_quantity"] == null
            ? null
            : json["line_order_quantity"],
        lineOrderTotal:
            json["line_order_total"] == null ? null : json["line_order_total"],
        onSiteDiscount:
            json["on_site_discount"] == null ? null : json["on_site_discount"],
        extraCharges:
            json["extra_charges"] == null ? null : json["extra_charges"],
        refundOrders: json["refund_orders"] == null
            ? null
            : List<RefundOrder>.from(
                json["refund_orders"].map((x) => RefundOrder.fromJson(x))),
        refundQuantity:
            json["refund_quantity"] == null ? null : json["refund_quantity"],
        refundAmount:
            json["refund_amount"] == null ? null : json["refund_amount"],
        invoiceNumberStart: json["invoice_number_start"] == null
            ? null
            : json["invoice_number_start"],
        invoiceNumberEnd: json["invoice_number_end"] == null
            ? null
            : json["invoice_number_end"],
        invoiceQuantity:
            json["invoice_quantity"] == null ? null : json["invoice_quantity"],
        invoiceAmount:
            json["invoice_amount"] == null ? null : json["invoice_amount"],
        invoiceVoidQuantity: json["invoice_void_quantity"] == null
            ? null
            : json["invoice_void_quantity"],
        invoiceVoidAmount: json["invoice_void_amount"] == null
            ? null
            : json["invoice_void_amount"],
        invoiceVoidNumber: json["invoice_void_number"] == null
            ? null
            : List<String>.from(json["invoice_void_number"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "subject": subject == null ? null : subject,
        "date": date == null ? null : date,
        "store_name": storeName == null ? null : storeName,
        "user": user == null ? null : user,
        "order_amount": orderAmount == null ? null : orderAmount,
        "order_tax": orderTax == null ? null : orderTax,
        "order_total": orderTotal == null ? null : orderTotal,
        "payment": payment == null
            ? null
            : List<dynamic>.from(payment.map((x) => x.toJson())),
        "order_number_start":
            orderNumberStart == null ? null : orderNumberStart,
        "order_number_end": orderNumberEnd == null ? null : orderNumberEnd,
        "app_order_quantity":
            appOrderQuantity == null ? null : appOrderQuantity,
        "app_order_total": appOrderTotal == null ? null : appOrderTotal,
        "line_order_quantity":
            lineOrderQuantity == null ? null : lineOrderQuantity,
        "line_order_total": lineOrderTotal == null ? null : lineOrderTotal,
        "on_site_discount": onSiteDiscount == null ? null : onSiteDiscount,
        "extra_charges": extraCharges == null ? null : extraCharges,
        "refund_orders": refundOrders == null
            ? null
            : List<dynamic>.from(refundOrders.map((x) => x.toJson())),
        "refund_quantity": refundQuantity == null ? null : refundQuantity,
        "refund_amount": refundAmount == null ? null : refundAmount,
        "invoice_number_start":
            invoiceNumberStart == null ? null : invoiceNumberStart,
        "invoice_number_end":
            invoiceNumberEnd == null ? null : invoiceNumberEnd,
        "invoice_quantity": invoiceQuantity == null ? null : invoiceQuantity,
        "invoice_amount": invoiceAmount == null ? null : invoiceAmount,
        "invoice_void_quantity":
            invoiceVoidQuantity == null ? null : invoiceVoidQuantity,
        "invoice_void_amount":
            invoiceVoidAmount == null ? null : invoiceVoidAmount,
        "invoice_void_number": invoiceVoidNumber == null
            ? null
            : List<dynamic>.from(invoiceVoidNumber.map((x) => x)),
      };
}

// class Payment {
//   Payment({
//     this.channelPayMethodName,
//     this.expenses,
//     this.income,
//   });

//   String channelPayMethodName;
//   num expenses;
//   num income;

//   Payment copyWith({
//     String channelPayMethodName,
//     num expenses,
//     num income,
//   }) =>
//       Payment(
//         channelPayMethodName: channelPayMethodName ?? this.channelPayMethodName,
//         expenses: expenses ?? this.expenses,
//         income: income ?? this.income,
//       );

//   factory Payment.fromRawJson(String str) => Payment.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory Payment.fromJson(Map<String, dynamic> json) => Payment(
//         channelPayMethodName: json["channel_pay_method_name"] == null
//             ? null
//             : json["channel_pay_method_name"],
//         expenses: json["expenses"] == null ? null : json["expenses"],
//         income: json["income"] == null ? null : json["income"],
//       );

//   Map<String, dynamic> toJson() => {
//         "channel_pay_method_name":
//             channelPayMethodName == null ? null : channelPayMethodName,
//         "expenses": expenses == null ? null : expenses,
//         "income": income == null ? null : income,
//       };
// }

// class RefundOrder {
//   RefundOrder({
//     this.invoiceNumber,
//     this.memberName,
//     this.orderNumber,
//     this.orderTotal,
//     this.paymentName,
//     this.refundCreatedAt,
//     this.refundStoreAccountName,
//   });

//   String invoiceNumber;
//   String memberName;
//   String orderNumber;
//   num orderTotal;
//   String paymentName;
//   String refundCreatedAt;
//   String refundStoreAccountName;

//   RefundOrder copyWith({
//     String invoiceNumber,
//     String memberName,
//     String orderNumber,
//     num orderTotal,
//     String paymentName,
//     String refundCreatedAt,
//     String refundStoreAccountName,
//   }) =>
//       RefundOrder(
//         invoiceNumber: invoiceNumber ?? this.invoiceNumber,
//         memberName: memberName ?? this.memberName,
//         orderNumber: orderNumber ?? this.orderNumber,
//         orderTotal: orderTotal ?? this.orderTotal,
//         paymentName: paymentName ?? this.paymentName,
//         refundCreatedAt: refundCreatedAt ?? this.refundCreatedAt,
//         refundStoreAccountName:
//             refundStoreAccountName ?? this.refundStoreAccountName,
//       );

//   factory RefundOrder.fromRawJson(String str) =>
//       RefundOrder.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory RefundOrder.fromJson(Map<String, dynamic> json) => RefundOrder(
//         invoiceNumber:
//             json["invoice_number"] == null ? null : json["invoice_number"],
//         memberName: json["member_name"] == null ? null : json["member_name"],
//         orderNumber: json["order_number"] == null ? null : json["order_number"],
//         orderTotal: json["order_total"] == null ? null : json["order_total"],
//         paymentName: json["payment_name"] == null ? null : json["payment_name"],
//         refundCreatedAt: json["refund_created_at"] == null
//             ? null
//             : json["refund_created_at"],
//         refundStoreAccountName: json["refund_store_account_name"] == null
//             ? null
//             : json["refund_store_account_name"],
//       );

//   Map<String, dynamic> toJson() => {
//         "invoice_number": invoiceNumber == null ? null : invoiceNumber,
//         "member_name": memberName == null ? null : memberName,
//         "order_number": orderNumber == null ? null : orderNumber,
//         "order_total": orderTotal == null ? null : orderTotal,
//         "payment_name": paymentName == null ? null : paymentName,
//         "refund_created_at": refundCreatedAt == null ? null : refundCreatedAt,
//         "refund_store_account_name":
//             refundStoreAccountName == null ? null : refundStoreAccountName,
//       };
// }
