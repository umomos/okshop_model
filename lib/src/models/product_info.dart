// To parse this JSON data, do
//
//     final productInfo = productInfoFromJson(jsonString);

import 'dart:convert';

import 'package:okshop_model/objectbox.g.dart';

// @Entity()
class ProductInfo {
  ProductInfo({
    this.categoryId,
    this.isAvailable,
    this.isVip,
    this.kind,
    this.price,
    this.productId,
    this.sort,
    this.stock,
    this.stockUnlimited,
    this.summary,
    this.title,
    this.vipPrice,
  });

  @Property(type: PropertyType.int)
  num categoryId;
  bool isAvailable;
  @Property(type: PropertyType.int)
  num isVip;
  @Property(type: PropertyType.int)
  num kind;
  @Property(type: PropertyType.float)
  num price;
  @Property(type: PropertyType.int)
  num productId;
  @Property(type: PropertyType.int)
  num sort;
  @Property(type: PropertyType.int)
  num stock;
  @Property(type: PropertyType.int)
  num stockUnlimited;
  String summary;
  String title;
  @Property(type: PropertyType.float)
  num vipPrice;
  @Unique(onConflict: ConflictStrategy.replace)
  String key;
  // @Id(assignable: true)
  // int id;

  ProductInfo copyWith({
    num categoryId,
    bool isAvailable,
    num isVip,
    num kind,
    num price,
    num productId,
    num sort,
    num stock,
    num stockUnlimited,
    String summary,
    String title,
    num vipPrice,
  }) =>
      ProductInfo(
        categoryId: categoryId ?? this.categoryId,
        isAvailable: isAvailable ?? this.isAvailable,
        isVip: isVip ?? this.isVip,
        kind: kind ?? this.kind,
        price: price ?? this.price,
        productId: productId ?? this.productId,
        sort: sort ?? this.sort,
        stock: stock ?? this.stock,
        stockUnlimited: stockUnlimited ?? this.stockUnlimited,
        summary: summary ?? this.summary,
        title: title ?? this.title,
        vipPrice: vipPrice ?? this.vipPrice,
      );

  factory ProductInfo.fromRawJson(String str) =>
      ProductInfo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductInfo.fromJson(Map<String, dynamic> json) => ProductInfo(
        categoryId: json["category_id"],
        isAvailable: json["is_available"],
        isVip: json["is_vip"],
        kind: json["kind"],
        price: json["price"],
        productId: json["product_id"],
        sort: json["sort"],
        stock: json["stock"],
        stockUnlimited: json["stock_unlimited"],
        summary: json["summary"],
        title: json["title"],
        vipPrice: json["vip_price"],
      );

  Map<String, dynamic> toJson() => {
        "category_id": categoryId,
        "is_available": isAvailable,
        "is_vip": isVip,
        "kind": kind,
        "price": price,
        "product_id": productId,
        "sort": sort,
        "stock": stock,
        "stock_unlimited": stockUnlimited,
        "summary": summary,
        "title": title,
        "vip_price": vipPrice,
      };
}
