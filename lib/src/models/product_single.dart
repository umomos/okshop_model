import 'dart:convert';

import 'products_image.dart';

class ProductSingle {
  ProductSingle({
    this.createdAt,
    this.id,
    this.price,
    this.stock,
    this.isAvailable = 1,
    this.stockUnlimited = 1,
    this.shippingType = 0,
    this.isHidden = 0,
    this.taxType = 1,
    this.summary,
    this.title,
    this.updatedAt,
    this.productCategories,
    this.productAdditionCategories,
    this.productImages,
    this.isVip = 0,
    this.vipPrice = 0,
  });

  String createdAt;
  int id;
  int price;
  int stock;

  // 售完/未售完設定(預設: 0, 零售用)
  //  0：售完
  //  1：未售完
  int isAvailable;

  // 庫存不限制(預設: 1)
  // 0：關閉
  // 1：開啟
  int stockUnlimited;

  // 物流類別(預設: 0)
  // 0：常溫
  // 1：低溫
  int shippingType;

  // 隱形商品
  // 0: 可見
  // 1: 隱藏
  num isHidden;

  // 商品稅金
  // 1: 應稅
  // 2: 免稅
  num taxType;

  // 是否為 vip 會員專屬產品
  num isVip;
  // 會員價格
  num vipPrice;

  String summary;
  String title;
  String updatedAt;
  List<ProductCategory> productCategories;
  List<ProductAdditionCategory> productAdditionCategories;
  List<ProductsImage> productImages;

  ProductSingle copyWith({
    String createdAt,
    int id,
    int isAvailable,
    int isHidden,
    int isVip,
    int price,
    List<ProductAdditionCategory> productAdditionCategories,
    List<ProductCategory> productCategories,
    List<ProductsImage> productImages,
    int shippingType,
    int stock,
    int stockUnlimited,
    String summary,
    int taxType,
    String title,
    String updatedAt,
    int vipPrice,
  }) =>
      ProductSingle(
        createdAt: createdAt ?? this.createdAt,
        id: id ?? this.id,
        isAvailable: isAvailable ?? this.isAvailable,
        isHidden: isHidden ?? this.isHidden,
        isVip: isVip ?? this.isVip,
        price: price ?? this.price,
        productAdditionCategories:
            productAdditionCategories ?? this.productAdditionCategories,
        productCategories: productCategories ?? this.productCategories,
        productImages: productImages ?? this.productImages,
        shippingType: shippingType ?? this.shippingType,
        stock: stock ?? this.stock,
        stockUnlimited: stockUnlimited ?? this.stockUnlimited,
        summary: summary ?? this.summary,
        taxType: taxType ?? this.taxType,
        title: title ?? this.title,
        updatedAt: updatedAt ?? this.updatedAt,
        vipPrice: vipPrice ?? this.vipPrice,
      );

  factory ProductSingle.fromRawJson(String str) =>
      ProductSingle.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  ProductSingle.fromJson(Map<String, dynamic> json) {
    createdAt = json['created_at'];
    id = json['id'];
    price = json['price'].toInt();
    stock = json['stock'];
    isHidden = json['is_hidden'] ?? 0;
    taxType = json['tax_type'] ?? 1;

    if (json['is_available'] != null) {
      isAvailable = json['is_available'];
    }
    if (json['stock_unlimited'] != null) {
      stockUnlimited = json['stock_unlimited'];
    }
    if (json['shipping_type'] != null) {
      shippingType = json['shipping_type'];
    }

    summary = json['summary'];
    title = json['title'];
    updatedAt = json['updated_at'];
    if (json['product_categories'] != null) {
      productCategories = [];
      json['product_categories'].forEach((v) {
        productCategories.add(new ProductCategory.fromJson(v));
      });
    }
    if (json['product_addition_categories'] != null) {
      productAdditionCategories = [];
      json['product_addition_categories'].forEach((v) {
        productAdditionCategories.add(new ProductAdditionCategory.fromJson(v));
      });
    }
    if (json['product_images'] != null) {
      productImages = [];
      json['product_images'].forEach((v) {
        productImages.add(new ProductsImage.fromJson(v));
      });
    }
    isVip = json["is_vip"] == null ? null : json["is_vip"];
    vipPrice = json["vip_price"] == null ? null : json["vip_price"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['created_at'] = this.createdAt;
    data['id'] = this.id;
    data['price'] = this.price;
    data['stock'] = this.stock;
    data['is_available'] = isAvailable;
    data['is_hidden'] = isHidden;
    data['tax_type'] = taxType;
    data['stock_unlimited'] = stockUnlimited;
    data['shipping_type'] = shippingType;
    data['summary'] = this.summary;
    data['title'] = this.title;
    data['updated_at'] = this.updatedAt;
    if (this.productCategories != null) {
      data['product_categories'] =
          this.productCategories.map((v) => v.toJson()).toList();
    }
    if (this.productAdditionCategories != null) {
      data['product_addition_categories'] =
          this.productAdditionCategories.map((v) => v.toJson()).toList();
    }
    if (this.productImages != null) {
      final ls = this.productImages.map((v) => v.toJson()).toList();
      data['product_images'] = jsonEncode(ls);
    }
    data["is_vip"] = isVip == null ? null : isVip;
    data["vip_price"] = vipPrice == null ? null : vipPrice;
    return data;
  }
}

class ProductAdditionCategory {
  ProductAdditionCategory({
    this.id,
    this.additionCategoryId,
    this.productId,
    this.title,
    this.option,
    this.optionMax,
    this.optionMin,
    this.required,
    this.createdAt,
  });

  int id;
  int additionCategoryId;
  int productId;
  String title;
  int option;
  int optionMax;
  int optionMin;
  int required;
  String createdAt;

  ProductAdditionCategory copyWith({
    int id,
    int additionCategoryId,
    int productId,
    String title,
    int option,
    int optionMax,
    int optionMin,
    int required,
    String createdAt,
  }) =>
      ProductAdditionCategory(
        id: id ?? this.id,
        additionCategoryId: additionCategoryId ?? this.additionCategoryId,
        productId: productId ?? this.productId,
        title: title ?? this.title,
        option: option ?? this.option,
        optionMax: optionMax ?? this.optionMax,
        optionMin: optionMin ?? this.optionMin,
        required: required ?? this.required,
        createdAt: createdAt ?? this.createdAt,
      );

  factory ProductAdditionCategory.fromRawJson(String str) =>
      ProductAdditionCategory.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductAdditionCategory.fromJson(Map<String, dynamic> json) =>
      ProductAdditionCategory(
        id: json["id"] == null ? null : json["id"],
        additionCategoryId: json["addition_category_id"] == null
            ? null
            : json["addition_category_id"],
        productId: json["product_id"] == null ? null : json["product_id"],
        title: json["title"] == null ? null : json["title"],
        option: json["option"] == null ? null : json["option"],
        optionMax: json["option_max"] == null ? null : json["option_max"],
        optionMin: json["option_min"] == null ? null : json["option_min"],
        required: json["required"] == null ? null : json["required"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "addition_category_id":
            additionCategoryId == null ? null : additionCategoryId,
        "product_id": productId == null ? null : productId,
        "title": title == null ? null : title,
        "option": option == null ? null : option,
        "option_max": optionMax == null ? null : optionMax,
        "option_min": optionMin == null ? null : optionMin,
        "required": required == null ? null : required,
        "created_at": createdAt == null ? null : createdAt,
      };
}

class ProductCategory {
  ProductCategory({
    this.id,
    this.categoryId,
    this.productId,
    this.createdAt,
  });

  int id;
  int categoryId;
  int productId;
  String createdAt;

  ProductCategory copyWith({
    int id,
    int categoryId,
    int productId,
    String createdAt,
  }) =>
      ProductCategory(
        id: id ?? this.id,
        categoryId: categoryId ?? this.categoryId,
        productId: productId ?? this.productId,
        createdAt: createdAt ?? this.createdAt,
      );

  factory ProductCategory.fromRawJson(String str) =>
      ProductCategory.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductCategory.fromJson(Map<String, dynamic> json) =>
      ProductCategory(
        id: json["id"] == null ? null : json["id"],
        categoryId: json["category_id"] == null ? null : json["category_id"],
        productId: json["product_id"] == null ? null : json["product_id"],
        createdAt: json["created_at"] == null ? null : json["created_at"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "category_id": categoryId == null ? null : categoryId,
        "product_id": productId == null ? null : productId,
        "created_at": createdAt == null ? null : createdAt,
      };
}
