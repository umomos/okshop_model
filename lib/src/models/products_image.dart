// To parse this JSON data, do
//
//     final productsImage = productsImageFromJson(jsonString);

import 'dart:convert';

class ProductsImage {
  ProductsImage({
    this.imageId,
    this.imageUrl,
    this.sort,
  });

  num imageId;
  String imageUrl;
  num sort;

  ProductsImage copyWith({
    num imageId,
    String imageUrl,
    num sort,
  }) =>
      ProductsImage(
        imageId: imageId ?? this.imageId,
        imageUrl: imageUrl ?? this.imageUrl,
        sort: sort ?? this.sort,
      );

  factory ProductsImage.fromRawJson(String str) =>
      ProductsImage.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ProductsImage.fromJson(Map<String, dynamic> json) => ProductsImage(
        imageId: json["image_id"] == null ? null : json["image_id"],
        imageUrl: json["image_url"] == null ? null : json["image_url"],
        sort: json["sort"] == null ? null : json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "image_id": imageId == null ? null : imageId,
        // "image_url": imageUrl == null ? null : imageUrl,
        "sort": sort == null ? null : sort,
      };
}
