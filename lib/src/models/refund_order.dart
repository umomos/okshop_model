// To parse this JSON data, do
//
//     final refundOrder = refundOrderFromJson(jsonString);

import 'dart:convert';

class RefundOrder {
  RefundOrder({
    this.invoiceNumber,
    this.memberName,
    this.orderNumber,
    this.orderTotal,
    this.paymentName,
    this.refundCreatedAt,
    this.refundStoreAccountName,
  });

  String invoiceNumber;
  String memberName;
  String orderNumber;
  num orderTotal;
  String paymentName;
  String refundCreatedAt;
  String refundStoreAccountName;

  RefundOrder copyWith({
    String invoiceNumber,
    String memberName,
    String orderNumber,
    num orderTotal,
    String paymentName,
    String refundCreatedAt,
    String refundStoreAccountName,
  }) =>
      RefundOrder(
        invoiceNumber: invoiceNumber ?? this.invoiceNumber,
        memberName: memberName ?? this.memberName,
        orderNumber: orderNumber ?? this.orderNumber,
        orderTotal: orderTotal ?? this.orderTotal,
        paymentName: paymentName ?? this.paymentName,
        refundCreatedAt: refundCreatedAt ?? this.refundCreatedAt,
        refundStoreAccountName:
            refundStoreAccountName ?? this.refundStoreAccountName,
      );

  factory RefundOrder.fromRawJson(String str) =>
      RefundOrder.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RefundOrder.fromJson(Map<String, dynamic> json) => RefundOrder(
        invoiceNumber:
            json["invoice_number"] == null ? null : json["invoice_number"],
        memberName: json["member_name"] == null ? null : json["member_name"],
        orderNumber: json["order_number"] == null ? null : json["order_number"],
        orderTotal: json["order_total"] == null ? null : json["order_total"],
        paymentName: json["payment_name"] == null ? null : json["payment_name"],
        refundCreatedAt: json["refund_created_at"] == null
            ? null
            : json["refund_created_at"],
        refundStoreAccountName: json["refund_store_account_name"] == null
            ? null
            : json["refund_store_account_name"],
      );

  Map<String, dynamic> toJson() => {
        "invoice_number": invoiceNumber == null ? null : invoiceNumber,
        "member_name": memberName == null ? null : memberName,
        "order_number": orderNumber == null ? null : orderNumber,
        "order_total": orderTotal == null ? null : orderTotal,
        "payment_name": paymentName == null ? null : paymentName,
        "refund_created_at": refundCreatedAt == null ? null : refundCreatedAt,
        "refund_store_account_name":
            refundStoreAccountName == null ? null : refundStoreAccountName,
      };
}
