// To parse this JSON data, do
//
//     final reportsStatements = reportsStatementsFromJson(jsonString);

import 'dart:convert';

import 'payment.dart';
import 'refund_order.dart';

class ReportsStatements {
  ReportsStatements({
    this.app,
    this.online,
  });

  App app;
  App online;

  ReportsStatements copyWith({
    App app,
    App online,
  }) =>
      ReportsStatements(
        app: app ?? this.app,
        online: online ?? this.online,
      );

  factory ReportsStatements.fromRawJson(String str) =>
      ReportsStatements.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory ReportsStatements.fromJson(Map<String, dynamic> json) =>
      ReportsStatements(
        app: json["app"] == null ? null : App.fromJson(json["app"]),
        online: json["online"] == null ? null : App.fromJson(json["online"]),
      );

  Map<String, dynamic> toJson() => {
        "app": app == null ? null : app.toJson(),
        "online": online == null ? null : online.toJson(),
      };
}

class App {
  App({
    this.extraCharges,
    this.invoiceAmount,
    this.invoiceNumberEnd,
    this.invoiceNumberStart,
    this.invoiceQuantity,
    this.invoiceVoidAmount,
    this.invoiceVoidNumber,
    this.invoiceVoidQuantity,
    this.onSiteDiscount,
    this.orderAmount,
    this.orderNumberEnd,
    this.orderNumberStart,
    this.orderQuantity,
    this.orderTax,
    this.orderTotal,
    this.payment,
    this.printLastDate,
    this.printStoreAccountName,
    this.refundAmount,
    this.refundOrders,
    this.refundQuantity,
    this.type,
    this.updatedAt,
  });

  num extraCharges;
  num invoiceAmount;
  String invoiceNumberEnd;
  String invoiceNumberStart;
  num invoiceQuantity;
  num invoiceVoidAmount;
  List<String> invoiceVoidNumber;
  num invoiceVoidQuantity;
  num onSiteDiscount;
  num orderAmount;
  String orderNumberEnd;
  String orderNumberStart;
  num orderQuantity;
  num orderTax;
  num orderTotal;
  List<Payment> payment;
  String printLastDate;
  String printStoreAccountName;
  num refundAmount;
  List<RefundOrder> refundOrders;
  num refundQuantity;
  num type;
  String updatedAt;

  App copyWith({
    num extraCharges,
    num invoiceAmount,
    String invoiceNumberEnd,
    String invoiceNumberStart,
    num invoiceQuantity,
    num invoiceVoidAmount,
    List<String> invoiceVoidNumber,
    num invoiceVoidQuantity,
    num onSiteDiscount,
    num orderAmount,
    String orderNumberEnd,
    String orderNumberStart,
    num orderQuantity,
    num orderTax,
    num orderTotal,
    List<Payment> payment,
    String printLastDate,
    String printStoreAccountName,
    num refundAmount,
    List<RefundOrder> refundOrders,
    num refundQuantity,
    num type,
    String updatedAt,
  }) =>
      App(
        extraCharges: extraCharges ?? this.extraCharges,
        invoiceAmount: invoiceAmount ?? this.invoiceAmount,
        invoiceNumberEnd: invoiceNumberEnd ?? this.invoiceNumberEnd,
        invoiceNumberStart: invoiceNumberStart ?? this.invoiceNumberStart,
        invoiceQuantity: invoiceQuantity ?? this.invoiceQuantity,
        invoiceVoidAmount: invoiceVoidAmount ?? this.invoiceVoidAmount,
        invoiceVoidNumber: invoiceVoidNumber ?? this.invoiceVoidNumber,
        invoiceVoidQuantity: invoiceVoidQuantity ?? this.invoiceVoidQuantity,
        onSiteDiscount: onSiteDiscount ?? this.onSiteDiscount,
        orderAmount: orderAmount ?? this.orderAmount,
        orderNumberEnd: orderNumberEnd ?? this.orderNumberEnd,
        orderNumberStart: orderNumberStart ?? this.orderNumberStart,
        orderQuantity: orderQuantity ?? this.orderQuantity,
        orderTax: orderTax ?? this.orderTax,
        orderTotal: orderTotal ?? this.orderTotal,
        payment: payment ?? this.payment,
        printLastDate: printLastDate ?? this.printLastDate,
        printStoreAccountName:
            printStoreAccountName ?? this.printStoreAccountName,
        refundAmount: refundAmount ?? this.refundAmount,
        refundOrders: refundOrders ?? this.refundOrders,
        refundQuantity: refundQuantity ?? this.refundQuantity,
        type: type ?? this.type,
        updatedAt: updatedAt ?? this.updatedAt,
      );

  factory App.fromRawJson(String str) => App.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory App.fromJson(Map<String, dynamic> json) => App(
        extraCharges:
            json["extra_charges"] == null ? null : json["extra_charges"],
        invoiceAmount:
            json["invoice_amount"] == null ? null : json["invoice_amount"],
        invoiceNumberEnd: json["invoice_number_end"] == null
            ? null
            : json["invoice_number_end"],
        invoiceNumberStart: json["invoice_number_start"] == null
            ? null
            : json["invoice_number_start"],
        invoiceQuantity:
            json["invoice_quantity"] == null ? null : json["invoice_quantity"],
        invoiceVoidAmount: json["invoice_void_amount"] == null
            ? null
            : json["invoice_void_amount"],
        invoiceVoidNumber: json["invoice_void_number"] == null
            ? null
            : List<String>.from(json["invoice_void_number"].map((x) => x)),
        invoiceVoidQuantity: json["invoice_void_quantity"] == null
            ? null
            : json["invoice_void_quantity"],
        onSiteDiscount:
            json["on_site_discount"] == null ? null : json["on_site_discount"],
        orderAmount: json["order_amount"] == null ? null : json["order_amount"],
        orderNumberEnd:
            json["order_number_end"] == null ? null : json["order_number_end"],
        orderNumberStart: json["order_number_start"] == null
            ? null
            : json["order_number_start"],
        orderQuantity:
            json["order_quantity"] == null ? null : json["order_quantity"],
        orderTax: json["order_tax"] == null ? null : json["order_tax"],
        orderTotal: json["order_total"] == null ? null : json["order_total"],
        payment: json["payment"] == null
            ? null
            : List<Payment>.from(
                json["payment"].map((x) => Payment.fromJson(x))),
        printLastDate:
            json["print_last_date"] == null ? null : json["print_last_date"],
        printStoreAccountName: json["print_store_account_name"] == null
            ? null
            : json["print_store_account_name"],
        refundAmount:
            json["refund_amount"] == null ? null : json["refund_amount"],
        refundOrders: json["refund_orders"] == null
            ? null
            : List<RefundOrder>.from(
                json["refund_orders"].map((x) => RefundOrder.fromJson(x))),
        refundQuantity:
            json["refund_quantity"] == null ? null : json["refund_quantity"],
        type: json["type"] == null ? null : json["type"],
        updatedAt: json["updated_at"] == null ? null : json["updated_at"],
      );

  Map<String, dynamic> toJson() => {
        "extra_charges": extraCharges == null ? null : extraCharges,
        "invoice_amount": invoiceAmount == null ? null : invoiceAmount,
        "invoice_number_end":
            invoiceNumberEnd == null ? null : invoiceNumberEnd,
        "invoice_number_start":
            invoiceNumberStart == null ? null : invoiceNumberStart,
        "invoice_quantity": invoiceQuantity == null ? null : invoiceQuantity,
        "invoice_void_amount":
            invoiceVoidAmount == null ? null : invoiceVoidAmount,
        "invoice_void_number": invoiceVoidNumber == null
            ? null
            : List<dynamic>.from(invoiceVoidNumber.map((x) => x)),
        "invoice_void_quantity":
            invoiceVoidQuantity == null ? null : invoiceVoidQuantity,
        "on_site_discount": onSiteDiscount == null ? null : onSiteDiscount,
        "order_amount": orderAmount == null ? null : orderAmount,
        "order_number_end": orderNumberEnd == null ? null : orderNumberEnd,
        "order_number_start":
            orderNumberStart == null ? null : orderNumberStart,
        "order_quantity": orderQuantity == null ? null : orderQuantity,
        "order_tax": orderTax == null ? null : orderTax,
        "order_total": orderTotal == null ? null : orderTotal,
        "payment": payment == null
            ? null
            : List<dynamic>.from(payment.map((x) => x.toJson())),
        "print_last_date": printLastDate == null ? null : printLastDate,
        "print_store_account_name":
            printStoreAccountName == null ? null : printStoreAccountName,
        "refund_amount": refundAmount == null ? null : refundAmount,
        "refund_orders": refundOrders == null
            ? null
            : List<dynamic>.from(refundOrders.map((x) => x.toJson())),
        "refund_quantity": refundQuantity == null ? null : refundQuantity,
        "type": type == null ? null : type,
        "updated_at": updatedAt == null ? null : updatedAt,
      };
}

// class Payment {
//   Payment({
//     this.channelPayMethodName,
//     this.expenses,
//     this.income,
//   });

//   String channelPayMethodName;
//   num expenses;
//   num income;

//   Payment copyWith({
//     String channelPayMethodName,
//     num expenses,
//     num income,
//   }) =>
//       Payment(
//         channelPayMethodName: channelPayMethodName ?? this.channelPayMethodName,
//         expenses: expenses ?? this.expenses,
//         income: income ?? this.income,
//       );

//   factory Payment.fromRawJson(String str) => Payment.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory Payment.fromJson(Map<String, dynamic> json) => Payment(
//         channelPayMethodName: json["channel_pay_method_name"] == null
//             ? null
//             : json["channel_pay_method_name"],
//         expenses: json["expenses"] == null ? null : json["expenses"],
//         income: json["income"] == null ? null : json["income"],
//       );

//   Map<String, dynamic> toJson() => {
//         "channel_pay_method_name":
//             channelPayMethodName == null ? null : channelPayMethodName,
//         "expenses": expenses == null ? null : expenses,
//         "income": income == null ? null : income,
//       };
// }

// class RefundOrder {
//   RefundOrder({
//     this.invoiceNumber,
//     this.memberName,
//     this.orderNumber,
//     this.orderTotal,
//     this.paymentName,
//     this.refundCreatedAt,
//     this.refundStoreAccountName,
//   });

//   String invoiceNumber;
//   String memberName;
//   String orderNumber;
//   num orderTotal;
//   String paymentName;
//   String refundCreatedAt;
//   String refundStoreAccountName;

//   RefundOrder copyWith({
//     String invoiceNumber,
//     String memberName,
//     String orderNumber,
//     num orderTotal,
//     String paymentName,
//     String refundCreatedAt,
//     String refundStoreAccountName,
//   }) =>
//       RefundOrder(
//         invoiceNumber: invoiceNumber ?? this.invoiceNumber,
//         memberName: memberName ?? this.memberName,
//         orderNumber: orderNumber ?? this.orderNumber,
//         orderTotal: orderTotal ?? this.orderTotal,
//         paymentName: paymentName ?? this.paymentName,
//         refundCreatedAt: refundCreatedAt ?? this.refundCreatedAt,
//         refundStoreAccountName:
//             refundStoreAccountName ?? this.refundStoreAccountName,
//       );

//   factory RefundOrder.fromRawJson(String str) =>
//       RefundOrder.fromJson(json.decode(str));

//   String toRawJson() => json.encode(toJson());

//   factory RefundOrder.fromJson(Map<String, dynamic> json) => RefundOrder(
//         invoiceNumber:
//             json["invoice_number"] == null ? null : json["invoice_number"],
//         memberName: json["member_name"] == null ? null : json["member_name"],
//         orderNumber: json["order_number"] == null ? null : json["order_number"],
//         orderTotal: json["order_total"] == null ? null : json["order_total"],
//         paymentName: json["payment_name"] == null ? null : json["payment_name"],
//         refundCreatedAt: json["refund_created_at"] == null
//             ? null
//             : json["refund_created_at"],
//         refundStoreAccountName: json["refund_store_account_name"] == null
//             ? null
//             : json["refund_store_account_name"],
//       );

//   Map<String, dynamic> toJson() => {
//         "invoice_number": invoiceNumber == null ? null : invoiceNumber,
//         "member_name": memberName == null ? null : memberName,
//         "order_number": orderNumber == null ? null : orderNumber,
//         "order_total": orderTotal == null ? null : orderTotal,
//         "payment_name": paymentName == null ? null : paymentName,
//         "refund_created_at": refundCreatedAt == null ? null : refundCreatedAt,
//         "refund_store_account_name":
//             refundStoreAccountName == null ? null : refundStoreAccountName,
//       };
// }
