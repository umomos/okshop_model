// To parse this JSON data, do
//
//     final revenue = revenueFromJson(jsonString);

import 'dart:convert';

class Revenue {
  Revenue({
    this.id,
    this.orderNumber,
    this.source,
    this.total,
    this.type,
  });

  num id;
  String orderNumber;
  num source;
  num total;
  num type;

  Revenue copyWith({
    num id,
    String orderNumber,
    num source,
    num total,
    num type,
  }) =>
      Revenue(
        id: id ?? this.id,
        orderNumber: orderNumber ?? this.orderNumber,
        source: source ?? this.source,
        total: total ?? this.total,
        type: type ?? this.type,
      );

  factory Revenue.fromRawJson(String str) => Revenue.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Revenue.fromJson(Map<String, dynamic> json) => Revenue(
        id: json["id"] == null ? null : json["id"],
        orderNumber: json["order_number"] == null ? null : json["order_number"],
        source: json["source"] == null ? null : json["source"],
        total: json["total"] == null ? null : json["total"],
        type: json["type"] == null ? null : json["type"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "order_number": orderNumber == null ? null : orderNumber,
        "source": source == null ? null : source,
        "total": total == null ? null : total,
        "type": type == null ? null : type,
      };
}
