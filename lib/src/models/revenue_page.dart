// To parse this JSON data, do
//
//     final revenuePage = revenuePageFromJson(jsonString);

import 'dart:convert';

import 'pagination.dart';
import 'revenue.dart';

class RevenuePage {
  RevenuePage({
    this.data,
    this.pagination,
    this.total,
  });

  List<Revenue> data;
  Pagination pagination;
  num total;

  RevenuePage copyWith({
    List<Revenue> data,
    Pagination pagination,
    num total,
  }) =>
      RevenuePage(
        data: data ?? this.data,
        pagination: pagination ?? this.pagination,
        total: total ?? this.total,
      );

  factory RevenuePage.fromRawJson(String str) =>
      RevenuePage.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RevenuePage.fromJson(Map<String, dynamic> json) => RevenuePage(
        data: json["data"] == null
            ? null
            : List<Revenue>.from(json["data"].map((x) => Revenue.fromJson(x))),
        pagination: json["pagination"] == null
            ? null
            : Pagination.fromJson(json["pagination"]),
        total: json["total"] == null ? null : json["total"],
      );

  Map<String, dynamic> toJson() => {
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
        "pagination": pagination == null ? null : pagination.toJson(),
        "total": total == null ? null : total,
      };
}
