// To parse this JSON data, do
//
//     final revenueReq = revenueReqFromJson(jsonString);

import 'dart:convert';

class RevenueReq {
  RevenueReq({
    this.page,
    this.limit,
    this.date,
  });

  num page;
  num limit;
  String date;

  RevenueReq copyWith({
    num page,
    num limit,
    String date,
  }) =>
      RevenueReq(
        page: page ?? this.page,
        limit: limit ?? this.limit,
        date: date ?? this.date,
      );

  factory RevenueReq.fromRawJson(String str) =>
      RevenueReq.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory RevenueReq.fromJson(Map<String, dynamic> json) => RevenueReq(
        page: json["page"] == null ? null : json["page"],
        limit: json["limit"] == null ? null : json["limit"],
        date: json["date"] == null ? null : json["date"],
      );

  Map<String, dynamic> toJson() => {
        "page": page == null ? null : page,
        "limit": limit == null ? null : limit,
        "date": date == null ? null : date,
      };
}
