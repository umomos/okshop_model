// To parse this JSON data, do
//
//     final settingLabel = settingLabelFromJson(jsonString);

import 'dart:convert';

class SettingLabel {
  SettingLabel({
    this.categoryIds,
    this.id,
    this.ip,
    this.macAddress,
    this.name,
    this.printCount,
    this.status,
    this.other,
    this.prnType,
    this.serialNo,
    this.printVoiceText,
  });

  List<num> categoryIds;
  num id;
  String ip;
  String macAddress;
  String name;
  num printCount;
  num status;
  Other other;
  String prnType;
  String serialNo;
  String printVoiceText;

  SettingLabel copyWith({
    List<num> categoryIds,
    num id,
    String ip,
    String macAddress,
    String name,
    num printCount,
    num status,
    Other other,
    String prnType,
    String serialNo,
    String printVoiceText,
  }) =>
      SettingLabel(
        categoryIds: categoryIds ?? this.categoryIds,
        id: id ?? this.id,
        ip: ip ?? this.ip,
        macAddress: macAddress ?? this.macAddress,
        name: name ?? this.name,
        printCount: printCount ?? this.printCount,
        status: status ?? this.status,
        other: other ?? this.other,
        prnType: prnType ?? this.prnType,
        serialNo: serialNo ?? this.serialNo,
        printVoiceText: printVoiceText ?? this.printVoiceText,
      );

  factory SettingLabel.fromRawJson(String str) =>
      SettingLabel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory SettingLabel.fromJson(Map<String, dynamic> json) => SettingLabel(
        categoryIds: json["category_ids"] == null
            ? null
            : List<num>.from(json["category_ids"].map((x) => x)),
        id: json["id"] == null ? null : json["id"],
        ip: json["ip"] == null ? null : json["ip"],
        macAddress: json["mac_address"] == null ? null : json["mac_address"],
        name: json["name"] == null ? null : json["name"],
        printCount: json["print_count"] == null ? null : json["print_count"],
        status: json["status"] == null ? null : json["status"],
        other:
            json["other"] == null ? Other() : Other.fromRawJson(json["other"]),
        prnType: json["prn_type"] == null ? null : json["prn_type"],
        serialNo: json["serial_no"] == null ? null : json["serial_no"],
        printVoiceText:
            json["print_voice_text"] == null ? null : json["print_voice_text"],
      );

  Map<String, dynamic> toJson() => {
        "category_ids": categoryIds == null
            ? null
            : List<dynamic>.from(categoryIds.map((x) => x)),
        "id": id == null ? null : id,
        "ip": ip == null ? null : ip,
        "mac_address": macAddress == null ? null : macAddress,
        "name": name == null ? null : name,
        "print_count": printCount == null ? null : printCount,
        "status": status == null ? null : status,
        "other": other == null ? "{}" : other.toRawJson(),
        "prn_type": prnType == null ? null : prnType,
        "serial_no": serialNo == null ? null : serialNo,
        "print_voice_text": printVoiceText == null ? null : printVoiceText,
      };
}

class Other {
  Other({
    this.type,
    this.categorySettings,
  });

  num type;
  Map<String, CategorySetting> categorySettings;

  Other copyWith({
    num type,
    Map<String, CategorySetting> categorySettings,
  }) =>
      Other(
        type: type ?? this.type,
        categorySettings: categorySettings ?? this.categorySettings,
      );

  factory Other.fromRawJson(String str) => Other.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Other.fromJson(Map<String, dynamic> json) => Other(
        type: json["type"] == null ? null : json["type"],
        categorySettings: json["category_settings"] == null
            ? null
            : Map.from(json["category_settings"]).map((k, v) =>
                MapEntry<String, CategorySetting>(
                    k, CategorySetting.fromJson(v))),
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "category_settings": categorySettings == null
            ? null
            : Map.from(categorySettings)
                .map((k, v) => MapEntry<String, dynamic>(k, v.toJson())),
      };
}

class CategorySetting {
  CategorySetting({
    this.printCount,
  });

  num printCount;

  CategorySetting copyWith({
    num printCount,
  }) =>
      CategorySetting(
        printCount: printCount ?? this.printCount,
      );

  factory CategorySetting.fromRawJson(String str) =>
      CategorySetting.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory CategorySetting.fromJson(Map<String, dynamic> json) =>
      CategorySetting(
        printCount: json["print_count"] == null ? null : json["print_count"],
      );

  Map<String, dynamic> toJson() => {
        "print_count": printCount == null ? null : printCount,
      };
}
