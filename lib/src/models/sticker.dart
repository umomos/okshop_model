// To parse this JSON data, do
//
//     final sticker = stickerFromJson(jsonString);

import 'dart:convert';

class Sticker {
  Sticker({
    this.tagStyle,
    this.headerTitleNum,
    this.headerTitleBlockText,
    this.headerTitleNote,
    this.headerSubTitle,
    this.headerSubTitleNote,
    this.bodyTitle,
    this.bodySubTitle,
  });

  num tagStyle;
  String headerTitleNum;
  String headerTitleBlockText;
  String headerTitleNote;
  String headerSubTitle;
  String headerSubTitleNote;
  String bodyTitle;
  String bodySubTitle;

  Sticker copyWith({
    num tagStyle,
    String headerTitleNum,
    String headerTitleBlockText,
    String headerTitleNote,
    String headerSubTitle,
    String headerSubTitleNote,
    String bodyTitle,
    String bodySubTitle,
  }) =>
      Sticker(
        tagStyle: tagStyle ?? this.tagStyle,
        headerTitleNum: headerTitleNum ?? this.headerTitleNum,
        headerTitleBlockText: headerTitleBlockText ?? this.headerTitleBlockText,
        headerTitleNote: headerTitleNote ?? this.headerTitleNote,
        headerSubTitle: headerSubTitle ?? this.headerSubTitle,
        headerSubTitleNote: headerSubTitleNote ?? this.headerSubTitleNote,
        bodyTitle: bodyTitle ?? this.bodyTitle,
        bodySubTitle: bodySubTitle ?? this.bodySubTitle,
      );

  factory Sticker.fromRawJson(String str) => Sticker.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Sticker.fromJson(Map<String, dynamic> json) => Sticker(
        tagStyle: json["tag_style"] == null ? null : json["tag_style"],
        headerTitleNum:
            json["header_title_num"] == null ? null : json["header_title_num"],
        headerTitleBlockText: json["header_title_block_text"] == null
            ? null
            : json["header_title_block_text"],
        headerTitleNote: json["header_title_note"] == null
            ? null
            : json["header_title_note"],
        headerSubTitle:
            json["header_sub_title"] == null ? null : json["header_sub_title"],
        headerSubTitleNote: json["header_sub_title_note"] == null
            ? null
            : json["header_sub_title_note"],
        bodyTitle: json["body_title"] == null ? null : json["body_title"],
        bodySubTitle:
            json["body_sub_title"] == null ? null : json["body_sub_title"],
      );

  Map<String, dynamic> toJson() => {
        "tag_style": tagStyle == null ? null : tagStyle,
        "header_title_num": headerTitleNum == null ? null : headerTitleNum,
        "header_title_block_text":
            headerTitleBlockText == null ? null : headerTitleBlockText,
        "header_title_note": headerTitleNote == null ? null : headerTitleNote,
        "header_sub_title": headerSubTitle == null ? null : headerSubTitle,
        "header_sub_title_note":
            headerSubTitleNote == null ? null : headerSubTitleNote,
        "body_title": bodyTitle == null ? null : bodyTitle,
        "body_sub_title": bodySubTitle == null ? null : bodySubTitle,
      };
}
