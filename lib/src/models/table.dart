// To parse this JSON data, do
//
//     final table = tableFromJson(jsonString);

import 'dart:convert';

class Table {
  Table({
    this.child,
    this.id,
    this.name,
    this.parentId,
    this.sort,
  });

  List<Table> child;
  num id;
  String name;
  num parentId;
  num sort;

  Table copyWith({
    List<Table> child,
    num id,
    String name,
    num parentId,
    num sort,
  }) =>
      Table(
        child: child ?? this.child,
        id: id ?? this.id,
        name: name ?? this.name,
        parentId: parentId ?? this.parentId,
        sort: sort ?? this.sort,
      );

  factory Table.fromRawJson(String str) => Table.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Table.fromJson(Map<String, dynamic> json) => Table(
        child: json["child"] == null
            ? null
            : List<Table>.from(json["child"].map((x) => Table.fromJson(x))),
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        parentId: json["parent_id"] == null ? null : json["parent_id"],
        sort: json["sort"] == null ? null : json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "child": child == null
            ? null
            : List<dynamic>.from(child.map((x) => x.toJson())),
        "id": id == null ? null : id,
        "name": name == null ? null : name,
        "parent_id": parentId == null ? null : parentId,
        "sort": sort == null ? null : sort,
      };
}
