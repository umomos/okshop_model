// To parse this JSON data, do
//
//     final tableReq = tableReqFromJson(jsonString);

import 'dart:convert';

class TableReq {
  TableReq({
    this.name,
    this.parentId,
    this.sort,
  });

  String name;
  num parentId;
  num sort;

  TableReq copyWith({
    String name,
    num parentId,
    num sort,
  }) =>
      TableReq(
        name: name ?? this.name,
        parentId: parentId ?? this.parentId,
        sort: sort ?? this.sort,
      );

  factory TableReq.fromRawJson(String str) =>
      TableReq.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory TableReq.fromJson(Map<String, dynamic> json) => TableReq(
        name: json["name"] == null ? null : json["name"],
        parentId: json["parent_id"] == null ? null : json["parent_id"],
        sort: json["sort"] == null ? null : json["sort"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "parent_id": parentId == null ? null : parentId,
        "sort": sort == null ? null : sort,
      };
}
