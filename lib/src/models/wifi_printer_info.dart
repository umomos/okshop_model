// To parse this JSON data, do
//
//     final wifiPrinterInfo = wifiPrinterInfoFromJson(jsonString);

import 'dart:convert';

class WifiPrinterInfo {
  WifiPrinterInfo({
    this.name,
    this.ip,
    this.port,
    this.macAddress,
  });

  String name;
  String ip;
  String port;
  String macAddress;

  WifiPrinterInfo copyWith({
    String name,
    String ip,
    String port,
    String macAddress,
  }) =>
      WifiPrinterInfo(
        name: name ?? this.name,
        ip: ip ?? this.ip,
        port: port ?? this.port,
        macAddress: macAddress ?? this.macAddress,
      );

  factory WifiPrinterInfo.fromRawJson(String str) =>
      WifiPrinterInfo.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory WifiPrinterInfo.fromJson(Map<String, dynamic> json) =>
      WifiPrinterInfo(
        name: json["name"] == null ? null : json["name"],
        ip: json["ip"] == null ? null : json["ip"],
        port: json["port"] == null ? null : json["port"],
        macAddress: json["mac_address"] == null ? null : json["mac_address"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "ip": ip == null ? null : ip,
        "port": port == null ? null : port,
        "mac_address": macAddress == null ? null : macAddress,
      };
}
