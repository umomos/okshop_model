import 'dart:convert';

import 'package:encrypt/encrypt.dart';

import 'constants.dart';

class Utils {
  static final _carrierId = RegExp(r'^/[\dA-Z+-\.]{7}$');
  static final _npoNab = RegExp(r'^\d{3,7}$');
  static final _taxId = RegExp(r'^\d{8}$');
  // 統一編號特定倍數
  static const _taxIdWeight = [1, 2, 1, 2, 1, 2, 4, 1];

  static String getEncryptedString({
    String aesKey,
    String invoiceNumber,
    String randomNumber,
  }) {
    final plainText = '$invoiceNumber$randomNumber';
    final key = Key.fromBase16(aesKey);
    final iv = IV.fromBase64(kIv);
    final encrypter = Encrypter(AES(key, mode: AESMode.cbc));
    final encrypted = encrypter.encrypt(plainText, iv: iv);
    // final result = encrypter.decrypt(encrypted, iv: iv);
    return encrypted.base64;
  }

  static String getBase64String(String value) {
    final bytes = utf8.encode(value ?? "");
    return base64Encode(bytes);
  }

  /// 驗證載具
  static bool isCarrierId(String value) {
    return _carrierId.hasMatch(value ?? "");
  }

  /// 驗證愛心碼
  static bool isNpoBan(String value) {
    return _npoNab.hasMatch(value ?? "");
  }

  /// 驗證統一編號
  /// http://www.skrnet.com/skrjs/demo/js0161.htm
  /// https://dotblogs.com.tw/ChentingW/2020/03/29/000036
  /// http://superlevin.ifengyuan.tw/%E7%87%9F%E5%88%A9%E4%BA%8B%E6%A5%AD%E7%B5%B1%E4%B8%80%E7%B7%A8%E8%99%9F%E9%82%8F%E8%BC%AF%E6%AA%A2%E6%9F%A5%E6%96%B9%E6%B3%95/
  static bool isTaxId(String input) {
    input ??= '';
    // 驗證內容為 8 碼數字
    if (_taxId.hasMatch(input) == false) {
      return false;
    }
    num sum = 0;
    final list = input.split('');
    for (var i = 0; i < list.length; i++) {
      final data = list.elementAt(i);
      // 個別乘上特定倍數,
      final subsum = num.parse(data) * _taxIdWeight.elementAt(i);
      // 若乘出來的值為二位數則將十位數和個位數相加
      sum += (subsum ~/ 10) + (subsum % 10);
    }
    if (0 == sum % 10) {
      return true;
    }
    // 若第7碼為 7, 再加上 1 被 10 整除也為正確
    if (7 == num.parse(list.elementAt(6))) {
      sum += 1;
      return 0 == sum % 10;
    }
    return false;
  }
}
