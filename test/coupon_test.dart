import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_model/okshop_model.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  Coupon input;

  setUp(() async {
    input = await rootBundle
        .loadString('assets/models/member.json')
        .then((value) => Coupon.fromRawJson(value));
  });

  tearDown(() {
    //
  });
}
