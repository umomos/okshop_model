import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_common/okshop_common.dart';
import 'package:okshop_model/okshop_model.dart';

main() {
  group('invoice', () {
    final invoiceNumber = 'AA12345678';
    final randomNumber = '1234';
    final seller = '83193989';
    final buyer = '12345678';
    final encrypted = 'vt2qXrGN7o/Xi2r5W1uADw==';
    final base64Item = '5ZWG5ZOB5LiA5om5OjE6MTAwMA==';
    final customField = ''.padRight(10, '*');
    final invoice = Invoice(
      invoiceNumber: invoiceNumber,
      randomNumber: randomNumber,
      seller: seller,
      buyer: buyer,
      dateTime: DateTime(2017, 12, 1, 18, 32, 1),
      items: [
        Item(
          itemName: '商品一批',
          quantity: 1,
          unitPrice: 1000,
          taxType: BpscmTaxType.TX.value,
        ),
      ],
    );

    test('length', () {
      final fixedString =
          '${invoiceNumber}1061201${randomNumber}000003B8000003E8$buyer$seller$encrypted';
      expect(invoice.fixedString, fixedString);
      expect(invoice.fixedString.length, kLeftStringFixedLength);
    });

    test('barcode', () {
      expect(invoice.barcode, '10612$invoiceNumber$randomNumber');
    });

    test('left string', () {
      final actual = invoice.leftString;
      expect(actual,
          '${invoiceNumber}1061201${randomNumber}000003B8000003E8$buyer$seller$encrypted:$customField:1:1:2:$base64Item');
    });

    test('right string', () {
      final actual = invoice.rightString;
      expect(actual, '**');
    });

    test('item length', () {
      final count = invoice.getMaxItemsCount(kRemainLength);
      final actual = invoice.getItemBase64String(0, count);
      expect(actual.length <= kRemainLength, true);
    });

    test('item', () {
      final count = invoice.getMaxItemsCount(kRemainLength);
      final actual = invoice.getItemBase64String(0, count);
      expect(actual, base64Item);
    });

    test('carrier id', () {
      expect(Utils.isCarrierId('/KA-+.01'), true);
      expect(Utils.isCarrierId('/kA-+.01'), false);
      expect(Utils.isCarrierId('/KA-+.0'), false);
      expect(Utils.isCarrierId('/KA-*.01'), false);
    });

    test('npo ban', () {
      expect(Utils.isNpoBan('000'), true);
      expect(Utils.isNpoBan('1234567'), true);
      expect(Utils.isNpoBan('01'), false);
      expect(Utils.isNpoBan('E234567'), false);
    });

    test('displayDate', () {
      expect(invoice.displayDate, '2017-12-01');
    });

    test('displayDateTime', () {
      expect(invoice.displayDateTime, '2017-12-01 18:32:01');
    });

    group('格式25', () {
      test('有', () {
        expect(invoice.displayTaxType, '格式25');
      });
      test('無', () {
        final invoice2 = Invoice();
        expect(invoice2.displayTaxType, '');
      });
    });

    test('隨機碼', () {
      expect(invoice.displayRandomNumber, '隨機碼1234');
    });

    test('總計', () {
      expect(invoice.displayTotalAmount, '總計1,000');
    });

    test('買方', () {
      expect(invoice.displayBuyer, '買方12345678');
    });

    test('賣方', () {
      expect(invoice.displaySeller, '賣方83193989');
    });

    test('tw date', () {
      expect(invoice.displayTwDateTime, '106年11-12月');
    });
  });
}
