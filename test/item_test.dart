import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_model/okshop_model.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  Item item;

  setUp(() async {
    await rootBundle.loadString('assets/models/invoice.json').then(
      (value) {
        // item = Item.fromRawJson(value);
        final invoice = Invoice.fromRawJson(value);
        item = invoice.items.first;
      },
    );
  });

  tearDown(() {
    //
  });

  test('invoiceString', () {
    expect(item.invoiceString, '商品一批:2:1000');
  });

  test('displayUnitPrice', () {
    expect(item.displayUnitPrice, '1,000');
  });

  test('leftString', () {
    expect(item.leftString, '  \$1,000 x 2');
  });

  test('rightString', () {
    expect(item.rightString, '2,000元 TX');
  });

  test('displayTotal', () {
    expect(item.displayTotal, '2,000');
  });

  test('totalAmount', () {
    expect(item.totalAmount, 2000);
  });

  test('freeSalesAmount', () {
    expect(item.freeSalesAmount, 0);
  });

  test('salesAmount', () {
    expect(item.salesAmount, 1905);
  });

  test('taxAmount', () {
    expect(item.taxAmount, 95);
  });
}
