import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_common/okshop_common.dart';
import 'package:okshop_model/okshop_model.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();

  Member input;

  setUp(() async {
    input = await rootBundle
        .loadString('assets/models/member.json')
        .then((value) => jsonDecode(value))
        .then((value) => Member.fromJson(value['data']));
  });

  tearDown(() {
    //
  });

  test('allCouponCount', () {
    expect(input.allCouponCount, 10);
  });

  test('get Dinner coupon count with App', () {
    expect(input.getCouponCount(StoreType.Dinner, OrderSource.App), 0);
  });

  test('get Dinner coupon count with Line', () {
    expect(input.getCouponCount(StoreType.Dinner, OrderSource.Line), 4);
  });

  test('get Retail coupon count with App', () {
    expect(input.getCouponCount(StoreType.Retail, OrderSource.App), 3);
  });

  test('get Retail coupon count with Line', () {
    expect(input.getCouponCount(StoreType.Retail, OrderSource.Line), 3);
  });
}
