import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_common/okshop_common.dart';
import 'package:okshop_model/okshop_model.dart';

main() {
  final receipt = Receipt();

  group('1. 於 APP 下單', () {
    test('1-1. 自取單', () {
      receipt.type = OrderType.RetailToGo.index;
      expect(receipt.target, '取貨');
      expect(receipt.needAddress, false);
    });
    test('1-2. 宅配單', () {
      receipt.type = OrderType.RetailDelivery.index;
      expect(receipt.target, '收件人');
      expect(receipt.needAddress, true);
    });
  });

  group('2. 線上下單', () {
    test('1-1. 自取單', () {
      receipt.type = OrderType.RetailToGo.index;
      expect(receipt.target, '取貨');
      expect(receipt.needAddress, false);
    });
    test('1-2. 宅配單', () {
      receipt.type = OrderType.RetailDelivery.index;
      expect(receipt.target, '收件人');
      expect(receipt.needAddress, true);
    });
  });

  group('1. 於 APP 下單', () {
    test('1-1. 內用', () {
      receipt.type = OrderType.DinnerHere.index;
      expect(receipt.target, '顧客');
      expect(receipt.needAddress, true);
    });
    test('1-2. 外送', () {
      receipt.type = OrderType.DinnerDelivery.index;
      expect(receipt.target, '顧客');
      expect(receipt.needAddress, true);
    });
    test('1-3. 外帶', () {
      receipt.type = OrderType.DinnerToGo.index;
      expect(receipt.target, '顧客');
      expect(receipt.needAddress, true);
    });
  });

  group('2. 線上下單', () {
    test('1-1. 內用', () {
      receipt.type = OrderType.DinnerHere.index;
      expect(receipt.target, '顧客');
      expect(receipt.needAddress, true);
    });
    test('1-2. 自取', () {
      receipt.type = OrderType.DinnerToGo.index;
      expect(receipt.target, '顧客');
      expect(receipt.needAddress, true);
    });
    test('1-3. 外送', () {
      receipt.type = OrderType.DinnerDelivery.index;
      expect(receipt.target, '顧客');
      expect(receipt.needAddress, true);
    });
  });
}
