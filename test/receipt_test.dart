import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_common/okshop_common.dart';
import 'package:okshop_model/okshop_model.dart';

void main() {
  TestWidgetsFlutterBinding.ensureInitialized();
  group('receipt', () {
    setUp(() {
      // Setup
    });

    tearDown(() {
      // TearDown
    });

    test('order number simply', () {
      final receipt = Receipt(orderNumber: '0123456789');
      expect(receipt.orderNumberSimply, '789');
    });

    group('invoice status', () {
      test('開立', () {
        final receipt = Receipt(invoiceStatus: InvoiceStatus.Invoice.index);
        expect(receipt.displayInvoiceStatus, '開立');
      });

      test('作廢', () {
        final receipt = Receipt(invoiceStatus: InvoiceStatus.Cancel.index);
        expect(receipt.displayInvoiceStatus, '作廢');
      });

      test('折讓', () {
        final receipt = Receipt(invoiceStatus: InvoiceStatus.Discount.index);
        expect(receipt.displayInvoiceStatus, '折讓');
      });
    });

    group('payment status', () {
      test('未付款', () {
        final receipt = Receipt(paymentStatus: PaymentStatus.Outstanding.index);
        expect(receipt.displayPaymentStatus, '未付款');
      });

      test('未結清', () {
        final receipt = Receipt(paymentStatus: PaymentStatus.Balance.index);
        expect(receipt.displayPaymentStatus, '未結清');
      });

      test('已付款', () {
        final receipt = Receipt(paymentStatus: PaymentStatus.Paid.index);
        expect(receipt.displayPaymentStatus, '已付款');
      });

      test('付款失敗', () {
        final receipt = Receipt(paymentStatus: PaymentStatus.Failed.index);
        expect(receipt.displayPaymentStatus, '付款失敗');
      });

      test('超過付款時間', () {
        final receipt = Receipt(paymentStatus: PaymentStatus.Timeout.index);
        expect(receipt.displayPaymentStatus, '超過付款時間');
      });
    });

    Future<String> _getJsonString() {
      return rootBundle.loadString('assets/models/receipt.json');
    }

    Future _getJson() => _getJsonString().then((value) => jsonDecode(value));

    Future<Receipt> _getInvoice() {
      return _getJsonString().then((value) => Receipt.fromRawJson(value));
      // return _getJson().then((value) => Receipt.fromJson(value));
    }

    test('displayCreatedAt', () async {
      final receipt = await _getInvoice();
      expect(receipt.displayCreatedAt, '2017-12-01 18:30:02');
    });

    test('displayCheckoutAt', () async {
      final receipt = await _getInvoice();
      expect(receipt.displayCheckoutAt, '12-01 18:31');
    });

    test('displayPrintAt', () async {
      final receipt = await _getInvoice();
      expect(receipt.displayPrintAt, '12-01 18:32');
    });
  });
}
