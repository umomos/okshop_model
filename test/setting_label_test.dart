import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_common/okshop_common.dart';
import 'package:okshop_model/okshop_model.dart';

const jsonString2 = '''
{
  "data": [{
    "category_ids": [
      141,
      139,
      138,
      140,
      142,
      143,
      146,
      147,
      149,
      150,
      148,
      151,
      152
    ],
    "id": 225,
    "ip": "192.168.50.8",
    "mac_address": "00:1D:9A:08:41:97",
    "name": "廚房",
    "print_count": 1,
    "status": 1
  }]
}
''';

const jsonString = '''
{
  "category_ids": [
    141,
    139,
    138,
    140,
    142,
    143,
    146,
    147,
    149,
    150,
    148,
    151,
    152
  ],
  "id": 225,
  "ip": "192.168.50.8",
  "mac_address": "00:1D:9A:08:41:97",
  "name": "廚房",
  "print_count": 1,
  "status": 1
}
''';

void main() {
  group('setting label test', () {
    test('description', () {
      final label = SettingLabel.fromRawJson(jsonString);
      expect(label.ip, '192.168.50.8');
    });
  });

  group('setting label', () {
    test('add duplicate category id', () {
      final label = SettingLabel();
      label.setHasCategory(1, true);
      label.setHasCategory(1, true);
      expect(label.categoryIds.length, 1);
    });

    test('add two category id', () {
      final label = SettingLabel();
      label.setHasCategory(1, true);
      label.setHasCategory(2, true);
      expect(label.categoryIds.length, 2);
    });

    test('remove category id', () {
      final label = SettingLabel();
      label.setHasCategory(1, true);
      label.setHasCategory(1, false);
      expect(label.categoryIds.length, 0);
    });

    test('remove not exist category id', () {
      final label = SettingLabel();
      label.setHasCategory(1, true);
      label.setHasCategory(2, false);
      expect(label.categoryIds.length, 1);
    });
  });

  group('parse ip address', () {
    test('ip address with port', () {
      const input = '192.168.1.1:9100';
      final sl = SettingLabel(ip: input);
      expect(sl.host, '192.168.1.1');
      expect(sl.port, 9100);
      expect(sl.type, '');
    });

    test('ip address without port', () {
      const input = '192.168.1.1';
      final sl = SettingLabel(ip: input);
      expect(sl.host, '192.168.1.1');
      expect(sl.port, null);
    });

    test('ip address empty', () {
      const input = '';
      final sl = SettingLabel(ip: input);
      expect(sl.host, '');
      expect(sl.port, null);
    });

    test('mac address with type', () {
      const input = 'esc@192.168.1.1:9100';
      final sl = SettingLabel(macAddress: input);
      expect(sl.macAddressOnly, '192.168.1.1:9100');
      expect(sl.type, 'esc');
      expect(sl.printerPaperSize, PrinterPaperSize.mm58);
    });

    test('mac address with paper size', () {
      const input = 'esc:2@192.168.1.1:9100';
      final sl = SettingLabel(macAddress: input);
      expect(sl.macAddressOnly, '192.168.1.1:9100');
      expect(sl.type, 'esc');
      expect(sl.printerPaperSize, PrinterPaperSize.mm80);
    });
  });
}
