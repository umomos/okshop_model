import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_common/okshop_common.dart';
import 'package:okshop_model/okshop_model.dart';

main() {
  // 折價品項
  final itemDiscount = Item(
    itemName: '現場折價',
    quantity: 1,
    unitPrice: -90.0,
    taxRate: 0.05,
    taxType: 1,
  );
  // 免稅品項
  final itemFreeTax = Item(
    itemName: '雞蛋',
    quantity: 1,
    unitPrice: 140.0,
    taxRate: 0.0,
    taxType: 3,
  );
  // 應稅品項
  final itemTXTax = Item(
    itemName: '可樂',
    quantity: 1,
    unitPrice: 100.0,
    taxRate: 0.05,
    taxType: 1,
  );

  final invoice = Invoice();

  group('混合稅率', () {
    test('item free', () {
      expect(itemFreeTax.taxAmount.round(), 0);
      expect(itemFreeTax.salesAmount.round(), 0);
      expect(itemFreeTax.freeSalesAmount.round(), 140);
      expect(itemFreeTax.totalAmount.round(), 140);
    });

    test('item TX', () {
      expect(itemTXTax.taxAmount.round(), 5);
      expect(itemTXTax.salesAmount.round(), 95);
      expect(itemTXTax.freeSalesAmount.round(), 0);
      expect(itemTXTax.totalAmount.round(), 100);
    });

    test('item discount', () {
      expect(itemDiscount.taxAmount.round(), 0);
      expect(itemDiscount.salesAmount.round(), 0);
      expect(itemDiscount.freeSalesAmount.round(), 0);
      expect(itemDiscount.totalAmount.round(), -90);
    });
  });

  group('invoice', () {
    test('all without buyer', () {
      invoice.buyer = '';
      invoice.items = [itemDiscount, itemFreeTax, itemTXTax];
      invoice.refresh();
      expect(invoice.hasBuyer, false);
      expect(invoice.taxType, BpscmTaxType.Mix.value);
      expect(invoice.itemsFreeSalesAmount.round(), 140);
      expect(invoice.itemsTXSalesAmount.round(), 10);
      expect(invoice.itemsSalesAmount.round(), 150);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsTotalAmount.round(), 150);
      // 直接取值，無運算
      // expect(invoice.displayTotalAmount, '總計 100');
    });

    test('all with buyer', () {
      invoice.buyer = '83193989';
      invoice.items = [itemDiscount, itemFreeTax, itemTXTax];
      invoice.refresh();
      expect(invoice.hasBuyer, true);
      expect(invoice.taxType, BpscmTaxType.Mix.value);
      expect(invoice.itemsFreeSalesAmount.round(), 140);
      expect(invoice.itemsTXSalesAmount.round(), 10);
      expect(invoice.itemsSalesAmount.round(), 150);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsTotalAmount.round(), 150);
    });

    test('mix', () {
      invoice.buyer = '83193989';
      invoice.items = [itemFreeTax, itemTXTax];
      invoice.refresh();
      expect(invoice.hasBuyer, true);
      expect(invoice.taxType, BpscmTaxType.Mix.value);
      expect(invoice.itemsFreeSalesAmount.round(), 140);
      expect(invoice.itemsTXSalesAmount.round(), 95);
      expect(invoice.itemsSalesAmount.round(), 235);
      expect(invoice.itemsTaxAmount.round(), 5);
      expect(invoice.itemsTotalAmount.round(), 240);
    });

    test('TX with buyer', () {
      invoice.buyer = '83193989';
      invoice.items = [itemTXTax];
      invoice.refresh();
      expect(invoice.hasBuyer, true);
      expect(invoice.taxType, BpscmTaxType.TX.value);
      expect(invoice.itemsFreeSalesAmount.round(), 0);
      expect(invoice.itemsTXSalesAmount.round(), 95);
      expect(invoice.itemsSalesAmount.round(), 95);
      expect(invoice.itemsTaxAmount.round(), 5);
      expect(invoice.itemsTotalAmount.round(), 100);
    });

    test('TX without buyer', () {
      invoice.buyer = '';
      invoice.items = [itemTXTax];
      invoice.refresh();
      expect(invoice.hasBuyer, false);
      expect(invoice.taxType, BpscmTaxType.TX.value);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsFreeSalesAmount.round(), 0);
      expect(invoice.itemsTXSalesAmount.round(), 100);
      expect(invoice.itemsSalesAmount.round(), 100);
      expect(invoice.itemsTotalAmount.round(), 100);
    });

    test('Free', () {
      invoice.buyer = '83193989';
      invoice.items = [itemFreeTax];
      invoice.refresh();
      expect(invoice.hasBuyer, true);
      expect(invoice.taxType, BpscmTaxType.Free.value);
      expect(invoice.itemsFreeSalesAmount.round(), 140);
      expect(invoice.itemsTXSalesAmount.round(), 0);
      expect(invoice.itemsSalesAmount.round(), 140);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsTotalAmount.round(), 140);
    });

    test('Discount', () {
      invoice.buyer = '83193989';
      invoice.items = [itemDiscount];
      invoice.refresh();
      expect(invoice.hasBuyer, true);
      expect(invoice.taxType, BpscmTaxType.TX.value);
      expect(invoice.itemsFreeSalesAmount.round(), 0);
      expect(invoice.itemsTXSalesAmount.round(), 0);
      expect(invoice.itemsSalesAmount.round(), 0);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsTotalAmount.round(), 0);
    });
  });

  group('稅額計算', () {
    test('狀況一： 當產品都是應稅時', () {
      final item1 = Item(
        itemName: 'A商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item2 = Item(
        itemName: 'B商品',
        quantity: 1,
        unitPrice: 200.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item3 = Item(
        itemName: '現場折扣',
        quantity: 1,
        unitPrice: -200.0,
        taxRate: 0.05,
        taxType: 1,
      );
      invoice.buyer = '83193989';
      invoice.items = [item1, item2, item3];
      invoice.refresh();
      expect(invoice.showTax, true);
      expect(invoice.itemsFreeSalesAmount.round(), 0);
      expect(invoice.itemsTXSalesAmount.round(), 286);
      expect(invoice.itemsSalesAmount.round(), 286);
      expect(invoice.itemsTaxAmount.round(), 14);
      expect(invoice.itemsTotalAmount.round(), 300);
    });
  });

  group('狀況二： 當商品包含應稅＆免稅時', () {
    test('2-1. 折扣金額 < 應稅商品', () {
      final item1 = Item(
        itemName: 'A商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.0,
        taxType: 3,
      );
      final item2 = Item(
        itemName: 'B商品',
        quantity: 1,
        unitPrice: 200.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item3 = Item(
        itemName: '現場折扣',
        quantity: 1,
        unitPrice: -100.0,
        taxRate: 0.05,
        taxType: 1,
      );

      invoice.buyer = '83193989';
      invoice.items = [item1, item2, item3];
      invoice.refresh();
      expect(invoice.showTax, true);
      expect(invoice.taxType, BpscmTaxType.Mix.value);
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      // expect(invoice.itemsTXSalesAmount.round(), 96);
      expect(invoice.itemsTXSalesAmount.round(), 95);
      // expect(invoice.itemsSalesAmount.round(), 396);
      expect(invoice.itemsSalesAmount.round(), 395);
      // expect(invoice.itemsTaxAmount.round(), 4);
      expect(invoice.itemsTaxAmount.round(), 5);
      expect(invoice.itemsTotalAmount.round(), 400);
    });

    test('2-2. 折扣金額 < 應稅商品', () {
      final item1 = Item(
        itemName: 'A商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.0,
        taxType: 3,
      );
      final item2 = Item(
        itemName: 'B商品',
        quantity: 1,
        unitPrice: 200.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item3 = Item(
        itemName: 'C商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item4 = Item(
        itemName: '現場折扣',
        quantity: 1,
        unitPrice: -400.0,
        taxRate: 0.05,
        taxType: 1,
      );
      invoice.buyer = '83193989';
      invoice.items = [item1, item2, item3, item4];
      invoice.refresh();
      expect(invoice.showTax, true);
      expect(invoice.taxType, BpscmTaxType.Mix.value);
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      expect(invoice.itemsTXSalesAmount.round(), 95);
      expect(invoice.itemsSalesAmount.round(), 395);
      expect(invoice.itemsTaxAmount.round(), 5);
      expect(invoice.itemsTotalAmount.round(), 400);
    });

    test('2-3. 折扣金額 > 應稅商品', () {
      final item1 = Item(
        itemName: 'A商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.0,
        taxType: 3,
      );
      final item2 = Item(
        itemName: 'B商品',
        quantity: 1,
        unitPrice: 200.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item3 = Item(
        itemName: 'C商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item4 = Item(
        itemName: '現場折扣',
        quantity: 1,
        unitPrice: -600.0,
        taxRate: 0.05,
        taxType: 1,
      );
      invoice.buyer = '83193989';
      invoice.items = [item1, item2, item3, item4];
      invoice.refresh();
      expect(invoice.showTax, true);
      expect(invoice.taxType, BpscmTaxType.Free.value);
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      expect(invoice.itemsTXSalesAmount.round(), 0);
      expect(invoice.itemsSalesAmount.round(), 200);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsTotalAmount.round(), 200);
    });
  });

  group('狀況三： 無統編，當商品包含應稅＆免稅時', () {
    test('2-1. 折扣金額 < 應稅商品', () {
      final item1 = Item(
        itemName: 'A商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.0,
        taxType: 3,
      );
      final item2 = Item(
        itemName: 'B商品',
        quantity: 1,
        unitPrice: 200.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item3 = Item(
        itemName: '現場折扣',
        quantity: 1,
        unitPrice: -100.0,
        taxRate: 0.05,
        taxType: 1,
      );

      invoice.buyer = '';
      invoice.items = [item1, item2, item3];
      invoice.refresh();
      expect(invoice.showTax, true);
      expect(invoice.taxType, BpscmTaxType.Mix.value);
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      // expect(invoice.itemsTXSalesAmount.round(), 96);
      expect(invoice.itemsTXSalesAmount.round(), 95);
      // expect(invoice.itemsSalesAmount.round(), 396);
      expect(invoice.itemsSalesAmount.round(), 395);
      // expect(invoice.itemsTaxAmount.round(), 4);
      expect(invoice.itemsTaxAmount.round(), 5);
      expect(invoice.itemsTotalAmount.round(), 400);
    });

    test('2-2. 折扣金額 < 應稅商品', () {
      final item1 = Item(
        itemName: 'A商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.0,
        taxType: 3,
      );
      final item2 = Item(
        itemName: 'B商品',
        quantity: 1,
        unitPrice: 200.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item3 = Item(
        itemName: 'C商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item4 = Item(
        itemName: '現場折扣',
        quantity: 1,
        unitPrice: -400.0,
        taxRate: 0.05,
        taxType: 1,
      );
      invoice.buyer = '';
      invoice.items = [item1, item2, item3, item4];
      invoice.refresh();
      expect(invoice.showTax, true);
      expect(invoice.taxType, BpscmTaxType.Mix.value);
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      expect(invoice.itemsTXSalesAmount.round(), 95);
      expect(invoice.itemsSalesAmount.round(), 395);
      expect(invoice.itemsTaxAmount.round(), 5);
      expect(invoice.itemsTotalAmount.round(), 400);
    });

    test('2-3. 折扣金額 > 應稅商品', () {
      final item1 = Item(
        itemName: 'A商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.0,
        taxType: 3,
      );
      final item2 = Item(
        itemName: 'B商品',
        quantity: 1,
        unitPrice: 200.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item3 = Item(
        itemName: 'C商品',
        quantity: 1,
        unitPrice: 300.0,
        taxRate: 0.05,
        taxType: 1,
      );
      final item4 = Item(
        itemName: '現場折扣',
        quantity: 1,
        unitPrice: -600.0,
        taxRate: 0.05,
        taxType: 1,
      );
      invoice.buyer = '';
      invoice.items = [item1, item2, item3, item4];
      invoice.refresh();
      expect(invoice.showTax, false);
      expect(invoice.taxType, BpscmTaxType.Free.value);
      expect(invoice.itemsFreeSalesAmount.round(), 300);
      expect(invoice.itemsTXSalesAmount.round(), 0);
      expect(invoice.itemsSalesAmount.round(), 200);
      expect(invoice.itemsTaxAmount.round(), 0);
      expect(invoice.itemsTotalAmount.round(), 200);
    });
  });
}
