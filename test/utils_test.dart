import 'package:flutter_test/flutter_test.dart';
import 'package:okshop_model/okshop_model.dart';

main() {
  group('Utils', () {
    test('正確統一編號', () {
      expect(Utils.isTaxId('83193989'), true);
    });

    test('錯誤統一編號', () {
      expect(Utils.isTaxId('12345678'), false);
    });

    test('第 7 個字元等於 7', () {
      expect(Utils.isTaxId('12345675'), true);
    });

    test('字串長度不足', () {
      expect(Utils.isTaxId('8319398'), false);
    });

    test('包含英文', () {
      expect(Utils.isTaxId('8319398g'), false);
    });
  });
}
